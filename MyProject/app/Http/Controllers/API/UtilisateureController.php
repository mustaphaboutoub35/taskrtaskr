<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UtilisateurRequest;
use App\Http\Resources\UtilisateurResource;
use App\Models\Utilisateur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UtilisateureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return UtilisateurResource::collection(Utilisateur::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UtilisateurRequest $request)
    {
        $formFields = $request->validated();
        $formFields["password"] = Hash::make($request->password);
        if ($request->hasFile("image")) {
            $formFields["image"] =  $request->file("image");
        } 
        $user = Utilisateur::create($formFields);
        return new UtilisateurResource($user);
    }

    /**
     * Display the specified resource.
     */
    public function show(Utilisateur $utilisateur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Utilisateur $utilisateur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Utilisateur $utilisateur)
    {
        //
    }
}
