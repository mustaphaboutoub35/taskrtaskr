<?php

namespace App\Http\Controllers;

use App\Models\Members;
use App\Models\Project;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // foreach(Project::all() as $pr){
        //     return $pr->members ;
        // }
        return Members::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Members $members, Request $request)
    {
        $formFilds = $request->all();
        $members = Members::create($formFilds);
        return $members;
    }

    /**
     * Display the specified resource.
     */
    public function show(Members $members)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Members $members)
    {
        $members = Members::find($request->member);
        $formFilds = $request->all();
        $members->fill($formFilds)->save();
        return $members;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Members $members,Request $request)
    {
        return Members::find($request->member)->delete();
    }
}
