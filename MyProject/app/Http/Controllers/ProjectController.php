<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Taches;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // return Project::all();
        // foreach (User::all() as $pr) {
        //     return $pr->project;
        // };
        return Project::all();
        // return Taches::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $formFilds = $request->all();
        $project = Project::create($formFilds);
        return $project;
    }

    /**
     * Display the specified resource.
     */
    public function show(Project $project)
    {
        return $project;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Project $project)
    {
        $formFilds = $request->all();
        $project->fill($formFilds)->save();
        return $project;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return response()->json([
            "message" => "le project est bien supprimé",
            "id" => $project->id
        ]);
    }
}
