<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Taches;
use App\Models\User;
use Illuminate\Http\Request;

class TachesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Taches::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $formFilds = $request->all();
        $taches = Taches::create($formFilds);
        return $taches;
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        return Taches::find($request->tach);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Taches $tache)
    {
        $tach = Taches::find($request->tach);
        $formFilds = $request->all();
        $tach->fill($formFilds)->save();
        return $tach;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Taches $taches, Request $request)
    {
        return Taches::find($request->tach)->delete();
    }
}
