<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UtilisateurResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {

        $values = parent::toArray($request);
        $values["password"] = $this->password;
        $values["image"] = "public/" . $values["image"];
        $values["created"] = date_format(date_create($values["created_at"]), "d-m-y");
        unset($values["created_at"]);
        return $values;
    }
}
