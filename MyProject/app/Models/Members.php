<?php

namespace App\Models;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Members extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "user_id",
        "project_id",
        "accept"
    ];

    public function members()
    {
        return $this->hasOne(User::class);
    }

    
    public function project()
    {
        return $this->hasOne(Project::class);
    }
}
