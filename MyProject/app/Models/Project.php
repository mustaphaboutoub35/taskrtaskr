<?php

namespace App\Models;

use App\Models\Members;
use App\Models\Taches;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "name",
        "user_id",
    ];

    public function taches()
    {
        return $this->hasMany(Taches::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
     public function members()
    {
        return $this->hasMany(Members::class);
    }
   
}
