<?php

namespace Database\Seeders;

use App\Models\Stage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Stage::factory(10)->create();
        // DB::table("stages")->insert([
        //     "nom" => "Mustapha" ,
        //     "prenom" => "Boutoub"
        // ]);
    }
}
