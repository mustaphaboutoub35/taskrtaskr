import React, { createContext, useContext, useState } from "react";
import UserApi from "../Service/Api/User/UserApi";
export const UserStateContext = createContext({
    user: {},
    projects: [],
    tasks: [],
    members: [],
    setMembers: () => {},
    GuestProject : [] ,
    setGuestProject : ()=>{} ,
    membersUser: [],
    setMembersUser: () => {},
    setTasks: () => {},
    authenticated: false,
    setUser: () => {},
    setProjects: () => {},
    logout: () => {},
    login: async (email, password) => {},
    register: async (name, email, password, password_confirmation) => {},
    postProject: async (name, user_id) => {},
    postTasks: async (title, description, state, project_id) => {},
    UpdateProject: async (id, name) => {},
    deleteProject: async (id) => {},
    deleteTask: async (id) => {},
    setAuthenticated: () => {},
    UpdateTask: async (id, title, description, state, project_id) => {},
    UpdateUser : async (id,name,email,password,password_confirmation,image)=>{},
    etat: {},
    setEtat: () => {},
});
export default function UserContext({ children }) {
    const [user, setUser] = useState({});
    const [projects, setProjects] = useState([]);
    const [tasks, setTasks] = useState([]);
    const [members, setMembers] = useState([]) ;
    const [membersUser, setMembersUser] = useState([]) ;
    const [GuestProject,setGuestProject] = useState([]) ; 
    const [etat, setEtat] = useState({
        pasEncour: [],
        enCours: [],
        termenee: [],
    });


    const [authenticated, _setAuthenticated] = useState(
        "true" === window.localStorage.getItem("AUTH")
    );
    const login = async (email, password) => {
        await UserApi.gitCsrfToken();
        return await UserApi.login(email, password);
    };
    const register = async (name, email, password, password_confirmation) => {
        await UserApi.gitCsrfToken();
        return await UserApi.register(
            name,
            email,
            password,
            password_confirmation
        );
    };
    const logout = () => {
        setUser({});
        setAuthenticated(false);
    };
    const postProject = async (name, user_id) => {
        await UserApi.gitCsrfToken();
        return await UserApi.postProject(name, user_id);
    };
    const postTasks = async (title, description, state, project_id) => {
        await UserApi.gitCsrfToken();
        return UserApi.postTasks(title, description, state, project_id);
    };
    const UpdateTask = async (id, title, description, state, project_id) => {
        await UserApi.gitCsrfToken();
        return await UserApi.UpdateProject(
            id,
            title,
            description,
            state,
            project_id
        );
    };
    const  UpdateUser = async (id,name,email,password,password_confirmation,image)=>{
        await UserApi.gitCsrfToken();
        return UserApi.UpdateUser(id,name,email,password,password_confirmation,image)
    }
    const UpdateProject = async (id, name) => {
        await UserApi.gitCsrfToken();
        return await UserApi.UpdateProject(id, name);
    };
    const deleteProject = async (id) => {
        await UserApi.gitCsrfToken();
        return await UserApi.deleteProject(id);
    };
    const deleteTask = async (id) => {
        await UserApi.gitCsrfToken();
        return await UserApi.deleteTask(id);
    };

    const setAuthenticated = (isAuthenticated) => {
        _setAuthenticated(isAuthenticated);
        window.localStorage.setItem("AUTH", isAuthenticated);
    };
    return (
        <>
            <UserStateContext.Provider
                value={{
                    user,
                    setUser,
                    membersUser,
                    setMembersUser,
                    authenticated,
                    GuestProject,
                    setGuestProject,
                    login,
                    logout,
                    register,
                    projects,
                    setProjects,
                    members,
                    setMembers,
                    tasks,
                    setTasks,
                    postProject,
                    UpdateProject,
                    deleteProject,
                    deleteTask,
                    postTasks,
                    UpdateTask,
                    setAuthenticated,
                    etat,
                    setEtat,
                    UpdateUser,
                }}
            >
                {children}
            </UserStateContext.Provider>
        </>
    );
}
export const useUserContext = () => useContext(UserStateContext);
