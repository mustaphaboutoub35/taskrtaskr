import { useEffect, useState } from "react";
import NaveBar from "../res/NaveBar";
import { AxiosClient } from "../../api/axios";
import { Link, useNavigate } from "react-router-dom";
import { useUserContext } from "../../Context/UserContext";
import UserApi from "../../Service/Api/User/UserApi";
import GuestProject from "./Gest/GuestProject";

export default function AddProject() {
    const context = useUserContext();
    const navigate = useNavigate();
    const [isloding, setIsLoding] = useState(true);
    const [info, setInfo] = useState({});
    const [currentId, setCurrentId] = useState("");
    const [forceRender, setForceRender] = useState(false);
    useEffect(() => {
        if (context.authenticated === true) {
            setIsLoding(true);
            UserApi.getUser()
                .then(({ data }) => {
                    setIsLoding(false);
                    context.setUser(data);
                    setInfo(data);
                    context.setUser(data);
                    UserApi.getProjects().then((values) => {
                        context.setProjects(
                            values.data.filter(
                                (e) => e.user_id == context.user.id
                            )
                        );
                        setForceRender(true);
                    });
                })
                .catch(() => {
                    context.setAuthenticated(false);
                    navigate("/login");
                });
        } else {
            navigate("/login");
        }
    }, [forceRender]);

    const handelCreateProject = () => {
        navigate("/createproject");
    };
    const handelDelete = () => {
        document.getElementById("deleteModal").classList.remove("hidden");
    };
    const handelTasks = (id) => {
        navigate(`/tasks/${id}`);
    };

    if (isloding) {
        return <></>;
    }

    return (
        <div>
            <NaveBar
                contnet={
                    <div className="  p-1 bg-white-50 text-medium text-gray-500 dark:text-gray-400 dark:bg-gray-800 rounded-lg w-full ">
                        <h3 className="text-lg font-bold text-gray-900 dark:text-white mb-2 float-rigth">
                            Project:
                        </h3>
                        <div className="flex justify-center items-center ">
                            <button
                                onClick={handelCreateProject}
                                type="button"
                                className="text-white bg-btn w-64 flex justify-center  hover:bg-btnHover focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 my-3"
                            >
                                <svg
                                    className="w-6 h-6 text-white dark:text-gray-500 mx-1"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                >
                                    <path
                                        stroke="currentColor"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="M12 7.757v8.486M7.757 12h8.486M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
                                    />
                                </svg>
                                Add project
                            </button>
                        </div>
                        {forceRender ? (
                            context.projects.map((project, key) => {
                                return (
                                    <div key={key}>
                                        <div className="p-2 my-2 bg-gray-50 text-medium text-gray-200 dark:text-gray-400 dark:bg-gray-800 rounded-lg w-full mt-1 hover:bg-gray-100">
                                            <div className=" grid scroll-m-1 grid-cols-1 md:grid-cols-2 items-center p-1">
                                                <div className=" flex items-center">
                                                    <svg
                                                        className="w-5 h-5 mb-2 ml-2 text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500"
                                                        aria-hidden="true"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        fill="currentColor"
                                                        viewBox="0 0 20 20"
                                                    >
                                                        <path d="M11.074 4 8.442.408A.95.95 0 0 0 7.014.254L2.926 4h8.148ZM9 13v-1a4 4 0 0 1 4-4h6V6a1 1 0 0 0-1-1H1a1 1 0 0 0-1 1v13a1 1 0 0 0 1 1h17a1 1 0 0 0 1-1v-2h-6a4 4 0 0 1-4-4Z" />
                                                        <path d="M19 10h-6a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1Zm-4.5 3.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2ZM12.62 4h2.78L12.539.41a1.086 1.086 0 1 0-1.7 1.352L12.62 4Z" />
                                                    </svg>
                                                    <p className="text-lg font-bold text-gray-900 dark:text-white text-left mb-1  ml-4 ">
                                                        Name : {project.name}
                                                    </p>
                                                </div>

                                                <div className=" flex justify-center md:justify-end mr-3 my-5 md:my-0 items-center  ">
                                                    <button
                                                        type="button"
                                                        onClick={() => {
                                                            handelTasks(
                                                                project.id
                                                            );
                                                        }}
                                                        className=" bg-green-100 text-green-800 text-sm font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-green-300 border border-green-400 flex justify-center items-center hover:opacity-90"
                                                    >
                                                        Enter in project
                                                        <svg
                                                            className="w-5 h-5 text-green-800 dark:text-white mx-1"
                                                            aria-hidden="true"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            width="24"
                                                            height="24"
                                                            fill="currentColor"
                                                            viewBox="0 0 24 24"
                                                        >
                                                            <path
                                                                fillRule="evenodd"
                                                                d="M14.516 6.743c-.41-.368-.443-1-.077-1.41a.99.99 0 0 1 1.405-.078l5.487 4.948.007.006A2.047 2.047 0 0 1 22 11.721a2.06 2.06 0 0 1-.662 1.51l-5.584 5.09a.99.99 0 0 1-1.404-.07 1.003 1.003 0 0 1 .068-1.412l5.578-5.082a.05.05 0 0 0 .015-.036.051.051 0 0 0-.015-.036l-5.48-4.942Zm-6.543 9.199v-.42a4.168 4.168 0 0 0-2.715 2.415c-.154.382-.44.695-.806.88a1.683 1.683 0 0 1-2.167-.571 1.705 1.705 0 0 1-.279-1.092V15.88c0-3.77 2.526-7.039 5.967-7.573V7.57a1.957 1.957 0 0 1 .993-1.838 1.931 1.931 0 0 1 2.153.184l5.08 4.248a.646.646 0 0 1 .012.011l.011.01a2.098 2.098 0 0 1 .703 1.57 2.108 2.108 0 0 1-.726 1.59l-5.08 4.25a1.933 1.933 0 0 1-2.929-.614 1.957 1.957 0 0 1-.217-1.04Z"
                                                                clipRule="evenodd"
                                                            />
                                                        </svg>
                                                    </button>
                                                    <button
                                                        type="button"
                                                        className="bg-indigo-100 text-indigo-800 text-sm font-medium me-2 px-2.5 py-0.5 rounded dark:bg-indigo-900 dark:text-red-300 border border-indigo-400 hover:opacity-90"
                                                    >
                                                        <Link
                                                            className="flex justify-center items-center"
                                                            to={`/editProject/${project.id}`}
                                                        >
                                                            Update
                                                            <svg
                                                                className="w-5 h-5 text-indigo-800 dark:text-white  mx-1"
                                                                aria-hidden="true"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="24"
                                                                height="24"
                                                                fill="currentColor"
                                                                viewBox="0 0 24 24"
                                                            >
                                                                <path
                                                                    fillRule="evenodd"
                                                                    d="M14 4.182A4.136 4.136 0 0 1 16.9 3c1.087 0 2.13.425 2.899 1.182A4.01 4.01 0 0 1 21 7.037c0 1.068-.43 2.092-1.194 2.849L18.5 11.214l-5.8-5.71 1.287-1.31.012-.012Zm-2.717 2.763L6.186 12.13l2.175 2.141 5.063-5.218-2.141-2.108Zm-6.25 6.886-1.98 5.849a.992.992 0 0 0 .245 1.026 1.03 1.03 0 0 0 1.043.242L10.282 19l-5.25-5.168Zm6.954 4.01 5.096-5.186-2.218-2.183-5.063 5.218 2.185 2.15Z"
                                                                    clipRule="evenodd"
                                                                />
                                                            </svg>
                                                        </Link>
                                                    </button>

                                                    <div>
                                                        <div className="flex justify-center">
                                                            <button
                                                                type="button"
                                                                className="bg-red-100 text-red-800 text-sm hover:opacity-90 font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 border border-red-400 flex items-center justify-center  "
                                                                onClick={() => {
                                                                    setCurrentId(
                                                                        project.id
                                                                    );
                                                                    handelDelete();
                                                                }}
                                                            >
                                                                Delete
                                                                <svg
                                                                    className="w-5 h-5 text-red-800 dark:text-white"
                                                                    aria-hidden="true"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="24"
                                                                    height="24"
                                                                    fill="none"
                                                                    viewBox="0 0 24 24"
                                                                >
                                                                    <path
                                                                        stroke="currentColor"
                                                                        strokeLinecap="round"
                                                                        strokeLinejoin="round"
                                                                        strokeWidth="2"
                                                                        d="M5 7h14m-9 3v8m4-8v8M10 3h4a1 1 0 0 1 1 1v3H9V4a1 1 0 0 1 1-1ZM6 7h12v13a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V7Z"
                                                                    />
                                                                </svg>
                                                            </button>
                                                        </div>

                                                        <div
                                                            id="deleteModal"
                                                            tabIndex="-1"
                                                            aria-hidden="true"
                                                            className="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 flex justify-center mt-5  w-full md:inset-0 h-modal md:h-full"
                                                        >
                                                            <div className="relative p-4 w-full max-w-md h-full md:h-auto">
                                                                <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                                                                    <button
                                                                        type="button"
                                                                        className="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                                                        data-modal-toggle="deleteModal"
                                                                    >
                                                                        <svg
                                                                            aria-hidden="true"
                                                                            id="handelX"
                                                                            onClick={() => {
                                                                                document
                                                                                    .getElementById(
                                                                                        "deleteModal"
                                                                                    )
                                                                                    .classList.add(
                                                                                        "hidden"
                                                                                    );
                                                                            }}
                                                                            className="w-5 h-5"
                                                                            fill="currentColor"
                                                                            viewBox="0 0 20 20"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                        >
                                                                            <path
                                                                                fillRule="evenodd"
                                                                                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                                                clipRule="evenodd"
                                                                            ></path>
                                                                        </svg>
                                                                        <span className="sr-only">
                                                                            Close
                                                                            modal
                                                                        </span>
                                                                    </button>
                                                                    <svg
                                                                        className="text-gray-400 dark:text-gray-500 w-11 h-11 mb-3.5 mx-auto"
                                                                        aria-hidden="true"
                                                                        fill="currentColor"
                                                                        viewBox="0 0 20 20"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                    >
                                                                        <path
                                                                            fillRule="evenodd"
                                                                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                                            clipRule="evenodd"
                                                                        ></path>
                                                                    </svg>
                                                                    <p className="mb-4 text-gray-500 dark:text-gray-300">
                                                                        Are you
                                                                        sure you
                                                                        want to
                                                                        delete
                                                                        this
                                                                        project?
                                                                    </p>
                                                                    <div className="flex justify-center items-center space-x-4">
                                                                        <button
                                                                            onClick={() => {
                                                                                document
                                                                                    .getElementById(
                                                                                        "deleteModal"
                                                                                    )
                                                                                    .classList.add(
                                                                                        "hidden"
                                                                                    );
                                                                            }}
                                                                            data-modal-toggle="deleteModal"
                                                                            type="button"
                                                                            className="py-2 px-3 text-sm font-medium text-gray-500 bg-white rounded-lg border border-gray-200 hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-primary-300 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                                                                        >
                                                                            No,
                                                                            cancel
                                                                        </button>
                                                                        <button
                                                                            onClick={() => {
                                                                                context.deleteProject(
                                                                                    currentId
                                                                                );
                                                                                document
                                                                                    .getElementById(
                                                                                        "deleteModal"
                                                                                    )
                                                                                    .classList.add(
                                                                                        "hidden"
                                                                                    );
                                                                                setForceRender(
                                                                                    false
                                                                                );
                                                                            }}
                                                                            type="submit"
                                                                            className="py-2 px-3 text-sm font-medium text-center text-white bg-red-600 rounded-lg hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-900"
                                                                        >
                                                                            Yes,
                                                                            I'm
                                                                            sure
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        ) : (
                            <div className="w-full my-5">
                                <div
                                    role="status"
                                    className="w-full p-4 space-y-4 border border-gray-200 divide-y divide-gray-200 rounded shadow animate-pulse dark:divide-gray-700 md:p-6 dark:border-gray-700 "
                                >
                                    <div className="flex items-center justify-between">
                                        <div>
                                            <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-600 w-24 mb-2.5"></div>
                                            <div className="w-32 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                                        </div>
                                        <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-12"></div>
                                    </div>
                                    <div className="flex items-center justify-between pt-4">
                                        <div>
                                            <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-600 w-24 mb-2.5"></div>
                                            <div className="w-32 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                                        </div>
                                        <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-12"></div>
                                    </div>
                                    <div className="flex items-center justify-between pt-4">
                                        <div>
                                            <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-600 w-24 mb-2.5"></div>
                                            <div className="w-32 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                                        </div>
                                        <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-12"></div>
                                    </div>
                                    <div className="flex items-center justify-between pt-4">
                                        <div>
                                            <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-600 w-24 mb-2.5"></div>
                                            <div className="w-32 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                                        </div>
                                        <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-12"></div>
                                    </div>
                                    <div className="flex items-center justify-between pt-4">
                                        <div>
                                            <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-600 w-24 mb-2.5"></div>
                                            <div className="w-32 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                                        </div>
                                        <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-12"></div>
                                    </div>
                                    <span className="sr-only">Loading...</span>
                                </div>
                            </div>
                        )}
                        <div>
                            <GuestProject />
                        </div>
                    </div>
                }
            />
        </div>
    );
}
