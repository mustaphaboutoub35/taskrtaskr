import React, { useEffect, useState } from "react";
import { useUserContext } from "../../Context/UserContext";
import NaveBar from "../res/NaveBar";
import { useNavigate } from "react-router-dom";

export default function CreateProject() {
    const context = useUserContext();
    const navigate = useNavigate();
    const [user, setUser] = useState(context.user.id);
    const [title, setTitle] = useState("");
    const [titleError, setTitleError] = useState("");

    useEffect(() => {
        if (context.authenticated === false) {
            context.logout();
            navigate("/login");
        }
    }, [context.authenticated]);

    const handelChangeInfo = (element) => {
        setTitle(element.target.value);
        // Reset title error when user starts typing
        setTitleError("");
    };

    const validateForm = () => {
        let isValid = true;
        if (title.trim() === "") {
            setTitleError("Please enter a project title");
            isValid = false;
        }
        return isValid;
    };

    const handelSubmit = async (e) => {
        e.preventDefault();
        if (validateForm()) {
            try {
                const response = await context.postProject(title, user);
                if (response && response.status === 201) {
                    navigate("/addProject");
                } else {
                    console.error("Project failed:", response);
                }
            } catch (error) {
                console.error("Project error:", error);
            }
        }
    };

    return (
        <div>
            <NaveBar
                contnet={
                    <div className="pr-6 pl-10 pt-8 mr-8 h-screen bg-gray-50 text-medium text-gray-500 dark:text-gray-400 bg-gray-100 rounded-lg w-full ">
                        <h3 className="text-lg font-bold text-gray-900 dark:text-black mb-10">
                            Add project :
                        </h3>
                        <form>
                            <div className="container mx-auto bg-gray-100 border py-5 rounded-lg">
                                <label htmlFor="#"> Name project :</label>{" "}
                                <br /> <br />
                                <input
                                    required
                                    onChange={handelChangeInfo}
                                    type="text"
                                    className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                    id="text"
                                    placeholder="Project de ..."
                                />{" "}
                                <br /> <br />
                                {titleError && (
                                    <p className="text-red-500">{titleError}</p>
                                )}
                                <div className=" flex justify-center items-center">
                                    <button
                                        onClick={handelSubmit}
                                        type="submit"
                                        className="bg-gray-100 text-gray-700 rounded-lg border border-gray-300 py-2 text-center w-40 hover:bg-gray-200 dark:hover:opacity-80"
                                    >
                                        create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                }
            />
        </div>
    );
}
