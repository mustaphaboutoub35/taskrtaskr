import React, { useEffect, useState } from "react";
import { useUserContext } from "../../Context/UserContext";
import { useNavigate, useParams } from "react-router-dom";
import UserApi from "../../Service/Api/User/UserApi";
import NaveBar from "../res/NaveBar";

export default function EditPro() {
    const context = useUserContext();
    const navigate = useNavigate();
    const { id } = useParams();
    const [title, setTitle] = useState("");
    const [titleError, setTitleError] = useState("");

    useEffect(() => {
        if (context.authenticated === false) {
            context.logout();
            navigate("/login");
        }
    }, [context.authenticated, navigate]);

    useEffect(() => {
        UserApi.getProjectParId(id)
            .then((response) => {
                setTitle(response.data);
            })
            .catch((error) => {
                console.error("Error fetching project:", error);
            });
    }, [id]);

    const handelChangeInfo = (element) => {
        setTitle(element.target.value);
        setTitleError(""); // Reset title error when user starts typing
    };

    const validateForm = () => {
        let isValid = true;
        if (title.trim() === "") {
            setTitleError("Please enter a project title");
            isValid = false;
        }
        return isValid;
    };

    const handelSubmit = async (e) => {
        e.preventDefault();
        if (validateForm()) {
            try {
                const response = await context.UpdateProject(id, title);
                console.log(response);
                if (response && response.status === 200) {
                    navigate("/addProject");
                } else {
                    console.error("Project update failed:", response);
                }
            } catch (error) {
                console.error("Project update error:", error);
            }
        }
    };

    const hanselBack = () => {
        navigate("/addProject");
    };

    return (
        <div>
            <NaveBar
                contnet={
                    <div className="pr-6 pl-10 pt-8 mr-8 h-screen bg-gray-50 text-medium text-gray-500 dark:text-gray-400 bg-gray-100 rounded-lg w-full ">
                        <h3 className="text-lg font-bold text-gray-900 dark:text-black mb-10">
                            <div className="flex justify-start items-start">
                                <button
                                    onClick={hanselBack}
                                    className="bg-gray-100 mt-0.5 px-3 rounded-md mx-5"
                                >
                                    <svg
                                        className="w-6 h-6 text-gray-900 dark:text-white"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M5 12h14M5 12l4-4m-4 4 4 4"
                                        />
                                    </svg>
                                </button>
                                <div>Edit project :</div>
                            </div>
                        </h3>
                        <form>
                            <div className="container mx-auto bg-gray-100 border py-5 rounded-lg">
                                <label htmlFor="#"> Name project :</label>{" "}
                                <br /> <br />
                                <input
                                    required
                                    defaultValue={title.name}
                                    onChange={handelChangeInfo}
                                    type="text"
                                    className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                    id="text"
                                    placeholder="Project de ..."
                                />
                                <br /> <br />
                                {titleError && (
                                    <p className="text-red-500">{titleError}</p>
                                )}
                                <div className="flex justify-center items-center">
                                    <button
                                        onClick={handelSubmit}
                                        type="submit"
                                        className="bg-gray-100 text-gray-700 rounded-lg border border-gray-300 py-2 text-center w-40 hover:bg-gray-200 dark:hover:opacity-80"
                                    >
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                }
            />
        </div>
    );
}
