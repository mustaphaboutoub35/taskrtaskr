import React, { useEffect, useState } from "react";
import { useUserContext } from "../../../Context/UserContext";
import UpdateState from "./UpdareState";

export default function TaskGst({ id, content, title, image, state }) {
    const [badge, setBadge] = useState("");
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const context = useUserContext();
    useEffect(() => {
        if (state === "pas_encore") {
            setBadge(
                "bg-red-100 text-red-800 inline-flex items-center  me-2 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 lg:hidden"
            );
        } else if (state === "en_cours") {
            setBadge(
                "bg-blue-100 text-blue-800 inline-flex items-center  me-2 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 lg:hidden"
            );
        } else {
            setBadge(
                "bg-green-100 text-green-800 inline-flex items-center  me-2 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 lg:hidden"
            );
        }
    }, [state]);

    const toggleDropdown = () => {
        setIsDropdownOpen(!isDropdownOpen);
    };

    return (
        <div className="max-w-md mb-3 z-50 mt-2 mx-auto ">
            <div className="bg-white border border-gray-200 rounded-lg shadow p-4 dark:bg-gray-800 dark:border-gray-700 hover:bg-opacity-80">
                <div className="flex items-center justify-between mb-4 float-right ">
                    <div className="flex items-center ">
                        <span className={badge}>
                            <svg
                                className="w-2.5 h-2.5 me-1.5"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                            >
                                <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm3.982 13.982a1 1 0 0 1-1.414 0l-3.274-3.274A1.012 1.012 0 0 1 9 10V6a1 1 0 0 1 2 0v3.586l2.982 2.982a1 1 0 0 1 0 1.414Z" />
                            </svg>
                            {state}
                        </span>
                        <button>
                            <img
                                className="w-8 h-8 border-2 border-white rounded-full dark:border-gray-800 mr-3"
                                src={image}
                                alt=""
                            />
                        </button>
                        <button
                            data-modal-target="crud-modal"
                            data-modal-toggle="crud-modal"
                            className="dark:text-blue-500"
                            onClick={toggleDropdown}
                        >
                            <svg
                                className="w-6 h-6 text-gray-800 dark:text-white"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                            >
                                <path d="M1 5h1.424a3.228 3.228 0 0 0 6.152 0H19a1 1 0 1 0 0-2H8.576a3.228 3.228 0 0 0-6.152 0H1a1 1 0 1 0 0 2Zm18 4h-1.424a3.228 3.228 0 0 0-6.152 0H1a1 1 0 1 0 0 2h10.424a3.228 3.228 0 0 0 6.152 0H19a1 1 0 0 0 0-2Zm0 6H8.576a3.228 3.228 0 0 0-6.152 0H1a1 1 0 0 0 0 2h1.424a3.228 3.228 0 0 0 6.152 0H19a1 1 0 0 0 0-2Z" />
                            </svg>
                        </button>
                    </div>
                </div>
                <h5 className="text-md font-medium font-bold leading-none text-gray-800 dark:text-white z-0 mx-4 my-4 w-44 ">
                    {title}
                </h5>
                <div className="flow-root">
                    <ul className="divide-y divide-gray-200 dark:divide-gray-700 bg-blue-50 px-2 rounded-sm">
                        <p className="py-3 sm:py-4">
                            {content}
                            <div>
                                <hr />
                                {isDropdownOpen && (
                                    <div className=" z-50 mt-2 w-50 bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600 p-2">
                                        <div className="px-4 py-3 text-sm text-gray-900 dark:text-white">
                                            <div className="font-medium truncate">
                                                started : 10/01/2024
                                            </div>

                                            <div className="font-medium truncate">
                                                finish : 10/02/2024
                                            </div>
                                        </div>
                                        <ul className="py-2 text-sm text-gray-700 dark:text-gray-200">
                                            <li className="border-b-2 border-gray-200 mb-1  bg-blue-50 block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                                                <UpdateState id={id} />
                                            </li>
                                        </ul>
                                    </div>
                                )}
                            </div>
                        </p>
                    </ul>
                </div>
            </div>
        </div>
    );
}
