import React, { useEffect, useRef, useState } from "react";
import profile1 from "../../../../public/image/profile/profile1.jpg";
import ProfileEllipse from "../../../../public/image/ProfileEllipse.png";
import ProfileEllipse2 from "../../../../public/image/ProfileEllipse2.png";
import ProfileEllipse3 from "../../../../public/image/ProfileEllipse3.png";
import ProfileRectangle9651 from "../../../../public/image/ProfileRectangle9651.png";
import UserApi from "../../../Service/Api/User/UserApi";
import { useUserContext } from "../../../Context/UserContext";
import { useParams } from "react-router-dom";
import TaskGst from "./TaskGst";
import DateTasks from "../Tasks/DateTasks";

export default function TasksGuest() {
    const context = useUserContext();
    const { project_id } = useParams();
    const [forcRender, setForcRender] = useState(false);
    const [myProject, setProject] = useState({});
    const toggleIsGray = () => {
        document.getElementById("isGray").classList.toggle("opacity-80");
    };

    const handelReder = (par) => {
        setForcRender(par);
    };

    useEffect(() => {
        UserApi.getTasks().then(({ data }) => {
            const filtData = data.filter((e) => e.project_id === +project_id);
            context.setTasks(filtData);
            context.setEtat((prevState) => {
                return {
                    ...prevState,
                    pasEncour: filtData.filter((e) => e.state === "pas_encore"),
                    enCours: filtData.filter((e) => e.state === "en_cours"),
                    termenee: filtData.filter((e) => e.state === "terminée"),
                };
            });
        });
        UserApi.getProjectParId(project_id).then(({ data }) => {
            setProject(data);
        });
    }, [forcRender]);

    return (
        <div>
            <h3 className="text-lg font-bold text-gray-900 dark:text-white mb-2 ">
                Today
            </h3>
            <p className="mb-2 font-bold">
                <DateTasks />
            </p>
            <div className=" bg-white rounded-xl py-4 px-4">
                <div className=" grid grid-col-2 xl:grid-cols-4 items-center">
                    <h3 className="font-bold text-xl text-gray-900 xl:mb-0 mb-3 ">
                        {myProject.name}
                    </h3>
                    <p></p>
                </div>
                <hr className=" m-4" />
                <div className=" lg:grid grid-cols-3 inline-flex rounded-md shadow-sm w-full  grid lg::grid-cols-3 text-center bg-blue-50 hidden">
                    <div
                        href="#"
                        aria-current="page"
                        className="px-4 py-2 text-sm font-medium text-gray-900  border border-gray-200 rounded-s-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 dark:focus:ring-blue-500 dark:focus:text-white"
                    >
                        PAS ENCORE
                    </div>
                    <div
                        href="#"
                        aria-current="page"
                        className="px-4 py-2 text-sm font-medium text-gray-900  border border-gray-200 rounded-s-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 dark:focus:ring-blue-500 dark:focus:text-white"
                    >
                        EN COURS
                    </div>
                    <div
                        href="#"
                        aria-current="page"
                        className="px-4 py-2 text-sm font-medium text-gray-900  border border-gray-200 rounded-s-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 dark:focus:ring-blue-500 dark:focus:text-white"
                    >
                        TERMINEE
                    </div>
                </div>
                <div className=" flex flex-row my-5 grid grid-cols-1  lg:grid-cols-3 rounded-md shadow-sm w-full bg-blue-50 py-2 ">
                    <div className="items-center mx-3">
                        {context.etat.pasEncour.map((e, key) => {
                            return (
                                <div key={key}>
                                    <TaskGst
                                        id={e.id}
                                        title={e.title}
                                        state={e.state}
                                        image={ProfileRectangle9651}
                                        content={e.description}
                                    />
                                </div>
                            );
                        })}
                    </div>
                    <div className="items-center mx-3">
                        {context.etat.enCours.map((e, key) => {
                            return (
                                <div key={key}>
                                    <TaskGst
                                        id={e.id}
                                        title={e.title}
                                        state={e.state}
                                        image={ProfileRectangle9651}
                                        content={e.description}
                                    />
                                </div>
                            );
                        })}
                    </div>
                    <div className="items-center mx-3">
                        {context.etat.termenee.map((e, key) => {
                            return (
                                <div key={key}>
                                    <TaskGst
                                        id={e.id}
                                        title={e.title}
                                        state={e.state}
                                        image={ProfileRectangle9651}
                                        content={e.description}
                                    />
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
}
