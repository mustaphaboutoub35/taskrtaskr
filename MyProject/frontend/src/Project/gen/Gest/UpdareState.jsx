import React, { useEffect, useState } from "react";
import { useUserContext } from "../../../Context/UserContext";
import { useNavigate, useParams } from "react-router-dom";
import UserApi from "../../../Service/Api/User/UserApi";

export default function UpdateState({ id, onReder, Render }) {
    const [crudModal, setCrudModal] = useState(false);
    const [infoTask, setInfoTask] = useState({
        state: "pas_encore",
    });

    useEffect(() => {
        UserApi.getTasks().then(async ({ data }) => {
            setInfoTask(await data.find((e) => e.id == id));
        });
    }, [crudModal]);

    const handelChangeInfo = (element) => {
        const { id, value } = element.target;
        setInfoTask((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    const handelSubmit = async (e) => {
        e.preventDefault();
            try {
                const response = await UserApi.UpdateGestTask(
                    id,
                    infoTask.state
                );
                if (response && response.status === 200) {
                    console.log(response);
                    setCrudModal(!crudModal);
                    onReder(!Render);
                    window.location.reload();
                } else {
                    console.error("Tasks failed:", response);
                }
            } catch (error) {
                console.error("Tasks error:", error);
            }
        
    };

    const tooglecrudModal = () => {
        setCrudModal(!crudModal);
    };

    return (
        <div>
            <button
                data-modal-target="crud-modal"
                data-modal-toggle="crud-modal"
                className=" w-full text-left"
                type="button"
                onClick={tooglecrudModal}
            >
                change state
            </button>
            {crudModal && (
                <div>
                    <div
                        id="crud-modal"
                        tabIndex="-1"
                        aria-hidden="true"
                        className=" overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full"
                    >
                        <div className="relative p-4 w-full max-w-sm mx-auto mt-20 lg:mr-36  max-h-full  ">
                            <div className="relative bg-white rounded-lg shadow-md border border-gray-300 dark:bg-gray-700">
                                <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
                                    <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                                        Update state of current Task
                                    </h3>
                                    <button
                                        type="button"
                                        className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                        data-modal-toggle="crud-modal"
                                        onClick={tooglecrudModal}
                                    >
                                        <svg
                                            className="w-3 h-3"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 14 14"
                                        >
                                            <path
                                                stroke="currentColor"
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                strokeWidth="2"
                                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                                            />
                                        </svg>
                                        <span className="sr-only">
                                            Close modal
                                        </span>
                                    </button>
                                </div>
                                <form className="p-4 md:p-5">
                                    <div className="grid gap-4 mb-4 grid-cols-1">
                                        <div className="col-span-2 sm:col-span-1">
                                            <label
                                                htmlFor="category"
                                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                            >
                                                Statut
                                            </label>
                                            <select
                                                id="state"
                                                name="state"
                                                onChange={handelChangeInfo}
                                                value={infoTask.state}
                                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark                                   :border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                            >
                                                <option value="pas_encore">
                                                    Pas encore
                                                </option>
                                                <option value="en_cours">
                                                    En cours
                                                </option>
                                                <option value="terminée">
                                                    Terminée
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <button
                                        type="submit"
                                        onClick={handelSubmit}
                                        className="
                                            text-white bg-btn  hover:opacity-80  focus:ring-4 focus:outline-none  font-medium rounded-lg text-sm px-4 py-2 
                                            text-white inline-flex items-center hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 w-50"
                                    >
                                        <svg
                                            className="me-1 -ms-1 w-5 h-5"
                                            fill="currentColor"
                                            viewBox="0 0 20 20"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path
                                                fillRule="evenodd"
                                                d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                                                clipRule="evenodd"
                                            ></path>
                                        </svg>
                                        Update new Task
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}
