import React, { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useUserContext } from "../../Context/UserContext";

export default function GuestLayout() {
    const navigate = useNavigate();
    const context = useUserContext();
    useEffect(() => {
        if (context.authenticated === true) {
            navigate("/addProject");
        }
    }, [context.authenticated]);
    return (
        <div>
            <Outlet />
        </div>
    );
}
