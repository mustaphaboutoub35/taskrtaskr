import { useContext, useEffect, useRef, useState } from "react";
import Section1 from "./start/Section/Section1";
import Section2 from "./start/Section/Section2";
import Section3 from "./start/Section/Section3";
import Section4 from "./start/Section/Section4";
import Section5 from "./start/Section/Section5";
import Section6 from "./start/Section/Section6";
import Section7 from "./start/Section/Section7";
import { useNavigate } from "react-router-dom";
import { useUserContext } from "../../../Context/UserContext";

export default function Home() {
    const navigate = useNavigate();
    const context = useUserContext();
    const Explore = useRef();
    const HomePage = useRef();
    const Contact = useRef();
    const [navB, setNavB] = useState("M1 1h15M1 7h15M1 13h15");
    const [menuHidden, setMenuHidden] = useState("hidden");
    const handelclick = () => {
        let menu2 = document.getElementById("menu2");
        menu2.classList.toggle(menuHidden);
        if (navB === "M1 1h15M1 7h15M1 13h15") {
            setNavB("m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6");
        } else {
            setNavB("M1 1h15M1 7h15M1 13h15");
        }
    };
    const handelLink = () => {
        let menu2 = document.getElementById("menu2");
        menu2.classList.toggle(menuHidden);
        if (navB === "M1 1h15M1 7h15M1 13h15") {
            setNavB("m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6");
        } else {
            setNavB("M1 1h15M1 7h15M1 13h15");
        }
    };

    const handelClickDown = () => {
        Explore.current.scrollIntoView({ behavior: "smooth" });
    };

    const handelClickDownContact = () => {
        Contact.current.scrollIntoView({ behavior: "smooth" });
    };

    const handelClickDownHom = () => {
        HomePage.current.scrollIntoView({ behavior: "smooth" });
    };

    const handelLogin = () => {
        navigate("/login");
    };
    return (
        <div>
            <nav className="bg-white dark:bg-gray-900 fixed w-full top-0   start-0 border-b border-gray-200 dark:border-gray-600  block bg-white z-50">
                <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
                    <a
                        href=""
                        className="flex items-center space-x-3 rtl:space-x-reverse"
                    >
                        <img
                            className="w-32"
                            src="public/image/TaskR.png"
                            alt=""
                        />
                    </a>
                    <div className="flex md:order-2 space-x-3 md:space-x-0 rtl:space-x-reverse">
                        <button
                            onClick={handelLogin}
                            type="button"
                            className="text-white bg-btn hover:opacity-80  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center flex "
                        >
                            <div className="mx-1">
                                {
                                    context.authenticated  ? 
                                    <p>Get started </p>
                                    : <p>log in</p>
                                }
                            </div>
                            <svg
                                className="w-6 h-6 text-white dark:text-white"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 18 16"
                            >
                                <path
                                    stroke="currentColor"
                                    strokeLinejoin="round"
                                    strokeLinecap="round"
                                    strokeWidth={2}
                                    d="M1 8h11m0 0L8 4m4 4-4 4m4-11h3a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-3"
                                />
                            </svg>
                        </button>

                        <div
                            id="hamburger"
                            className="cursor-pointer md:hidden text-white  hover:opacity-80  focus:ring-4 focus:outline-none focus:ring-btn-300 px-4 py-2 "
                        >
                            <svg
                                className="w-6 h-6 text-gray-800 dark:text-white"
                                aria-hidden="true"
                                fill="none"
                                viewBox="0 0 17 14"
                                id="carer"
                                onClick={handelclick}
                            >
                                <path
                                    stroke="currentColor"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth="2"
                                    d={navB}
                                />
                            </svg>
                        </div>
                    </div>
                    {/* menu */}
                    <div
                        className="items-center justify-between hidden w-full md:flex md:w-auto md:order-1"
                        id="navbar-sticky"
                    >
                        <ul className="flex flex-col p-4 md:p-0 mt-4 font-medium border border-gray-100 rounded-lg bg-gray-50 md:space-x-20 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                            <li>
                                <button
                                    onClick={handelClickDownHom}
                                    className=" py-2 px-3  text-gray-800  hover:text-btn focus:text-btn rounded  dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 flex"
                                >
                                    <svg
                                        className="w-5 h-5 mb-2 dark:group-hover:text-blue-500 relative right-2"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                    >
                                        <path d="m19.707 9.293-2-2-7-7a1 1 0 0 0-1.414 0l-7 7-2 2a1 1 0 0 0 1.414 1.414L2 10.414V18a2 2 0 0 0 2 2h3a1 1 0 0 0 1-1v-4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v4a1 1 0 0 0 1 1h3a2 2 0 0 0 2-2v-7.586l.293.293a1 1 0 0 0 1.414-1.414Z" />
                                    </svg>
                                    Home
                                </button>
                            </li>
                            <li>
                                <button
                                    onClick={handelClickDown}
                                    href="#"
                                    className=" py-2 px-3  text-gray-800  hover:text-btn focus:text-btn rounded  dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 flex"
                                >
                                    <svg
                                        className="w-6 h-6 dark:text-white relative right-2 "
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 20 20"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinejoin="round"
                                            strokeLinecap="round"
                                            strokeWidth={2}
                                            d="M5 1v3m5-3v3m5-3v3M1 7h18M5 11h10M2 3h16a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1Z"
                                        />
                                    </svg>
                                    About
                                </button>
                            </li>
                            <li>
                                <button
                                    onClick={handelClickDownContact}
                                    href="#"
                                    className=" py-2 px-3  text-gray-800  hover:text-btn focus:text-btn rounded  dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 flex"
                                >
                                    <svg
                                        className="w-5 h-5 mb-2 dark:group-hover:text-blue-500  relative top-1 right-2"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                    >
                                        <path d="m10.036 8.278 9.258-7.79A1.979 1.979 0 0 0 18 0H2A1.987 1.987 0 0 0 .641.541l9.395 7.737Z" />
                                        <path d="M11.241 9.817c-.36.275-.801.425-1.255.427-.428 0-.845-.138-1.187-.395L0 2.6V14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2.5l-8.759 7.317Z" />
                                    </svg>
                                    Contact
                                </button>
                            </li>
                        </ul>
                    </div>
                    {/* menu  */}

                    <div
                        className="items-center justify-between hidden w-full   md:w-auto md:order-1 h-[100vh] z-50"
                        id="menu2"
                    >
                        <ul className="flex flex-col items-center md:hidden p-4 md:p-0 mt-4 font-medium border border-gray-100 rounded-lg bg-gray-50 md:space-x-20 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700 ">
                            <li className="flex bg-gray-200 w-full justify-center items-center rounded-lg py-2 my-2">
                                <a
                                    href="#"
                                    onClick={handelLink}
                                    className=" py-2 px-3  text-gray-800  hover:text-btn focus:text-btn rounded  dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 flex "
                                >
                                    <svg
                                        className="w-5 h-5 mb-2 dark:group-hover:text-blue-500 relative right-2"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                    >
                                        <path d="m19.707 9.293-2-2-7-7a1 1 0 0 0-1.414 0l-7 7-2 2a1 1 0 0 0 1.414 1.414L2 10.414V18a2 2 0 0 0 2 2h3a1 1 0 0 0 1-1v-4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v4a1 1 0 0 0 1 1h3a2 2 0 0 0 2-2v-7.586l.293.293a1 1 0 0 0 1.414-1.414Z" />
                                    </svg>
                                    Home
                                </a>
                            </li>
                            <li className="flex bg-gray-200 w-full justify-center items-center rounded-lg py-2 my-2 ">
                                <a
                                    href="#"
                                    onClick={(e) => {
                                        handelLink(e);
                                        handelClickDown(e);
                                    }}
                                    className=" py-2 px-3  text-gray-800  hover:text-btn focus:text-btn rounded  dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 flex"
                                >
                                    <svg
                                        className="w-6 h-6 dark:text-white relative right-2 "
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 20 20"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinejoin="round"
                                            strokeLinecap="round"
                                            strokeWidth={2}
                                            d="M5 1v3m5-3v3m5-3v3M1 7h18M5 11h10M2 3h16a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1Z"
                                        />
                                    </svg>
                                    About
                                </a>
                            </li>
                            <li className="flex bg-gray-200 w-full justify-center items-center rounded-lg py-2 my-2">
                                <a
                                    href="#"
                                    onClick={(e) => {
                                        handelLink(e);
                                        handelClickDownContact(e);
                                    }}
                                    className=" py-2 px-3  text-gray-800  hover:text-btn focus:text-btn rounded  dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700 flex"
                                >
                                    <svg
                                        className="w-5 h-5 mb-2 dark:group-hover:text-blue-500  relative top-1 right-2"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                    >
                                        <path d="m10.036 8.278 9.258-7.79A1.979 1.979 0 0 0 18 0H2A1.987 1.987 0 0 0 .641.541l9.395 7.737Z" />
                                        <path d="M11.241 9.817c-.36.275-.801.425-1.255.427-.428 0-.845-.138-1.187-.395L0 2.6V14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2.5l-8.759 7.317Z" />
                                    </svg>
                                    Contact
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div className=" ">
                <div
                    ref={HomePage}
                    className=" container-fluid mx-5 md:container my-12"
                >
                    <main>
                        <div>
                            <Section1 />
                        </div>

                        <hr />
                        <div ref={Explore}>
                            <Section2 />
                        </div>
                        <Section3 />
                        <hr />
                        <Section4 />
                        <hr />
                        <Section5 />
                        <hr />
                        <Section6 />
                        <hr />
                        <Section7 />
                        <hr />
                        <footer
                            ref={Contact}
                            className="bg-white rounded-lg shadow m-4 dark:bg-gray-800"
                        >
                            <div className="w-full mx-auto max-w-screen-xl p-4 md:flex md:items-center md:justify-between">
                                <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
                                    © 2024{" "}
                                    <a
                                        href="https://TaskR/"
                                        className="hover:underline"
                                    >
                                        TaskR™
                                    </a>
                                    . All Rights Reserved.
                                </span>
                                <ul className="flex flex-wrap items-center mt-3 text-sm font-medium text-gray-500 dark:text-gray-400 sm:mt-0">
                                    <li>
                                        <button
                                            onClick={handelClickDown}
                                            className="hover:underline me-4 md:me-6"
                                        >
                                            About
                                        </button>
                                    </li>
                                    <li className=" mx-5">
                                        <a href="#" className="hover:underline">
                                            Facebook
                                        </a>
                                    </li>
                                    <li className=" mx-5">
                                        <a href="#" className="hover:underline">
                                            Email
                                        </a>
                                    </li>
                                    <li className=" mx-5">
                                        <a href="#" className="hover:underline">
                                            Linkdin
                                        </a>
                                    </li>
                                    <li className=" mx-5">
                                        <a href="#" className="hover:underline">
                                            Git hub
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </footer>
                    </main>
                </div>
            </div>
        </div>
    );
}
