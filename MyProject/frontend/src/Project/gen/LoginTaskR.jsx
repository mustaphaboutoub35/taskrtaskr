import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useUserContext } from "../../Context/UserContext";

export default function LoginTaskR() {
    const navigate = useNavigate();
    const useSignin = () => {
        navigate("/signup");
    };
    const [info, setInfo] = useState({ email: "", password: "" });
    const [remember, setRemember] = useState(false);
    const [errors, setErrors] = useState({}); // State for error messages
    const context = useUserContext();
    const [load, setLoad] = useState(false);

    useEffect(() => {
        if (context.authenticated === "true") {
            navigate("/addProject");
        }
    }, []);

    const handelChangeInfo = (element) => {
        const { id, value } = element.target;
        setInfo((prevState) => ({
            ...prevState,
            [id]: value,
        }));
        // Reset error message when user starts typing
        setErrors((prevState) => ({
            ...prevState,
            [id]: "",
        }));
    };

    const handelRememberChange = (event) => {
        setRemember(event.target.checked);
    };

    const validateForm = () => {
        let valid = true;
        const errorsObj = {};

        if (!info.email) {
            errorsObj.email = "Please enter your email";
            valid = false;
        }

        if (!info.password) {
            errorsObj.password = "Please enter your password";
            valid = false;
        }

        setErrors(errorsObj);
        return valid;
    };

    const handelSubmit = async (e) => {
        e.preventDefault();

        if (!validateForm()) {
            return;
        }
        
        document.getElementById("login").classList.remove("hidden");

        try {
            const response = await context.login(
                info.email,
                info.password,
                remember
            );
            if (response && response.status === 204) {
        document.getElementById("login").classList.add("hidden");
                setLoad(true);
                context.setAuthenticated(true);
                navigate("/addproject");
            }
        } catch (error) {
            document.getElementById("login").classList.add("hidden");
            setErrors((prevState) => {
                return { ...prevState, email:"your email or password are not valid" };
            });
            console.error("Login error:", error.response.data.message);
        }
    };

    const handleBack = () => {
        navigate("/");
    };

    return (
        <div className="container">
            <button
                onClick={handleBack}
                className="bg-gray-100 px-3 rounded-md relative top-10"
            >
                <svg
                    className="w-6 h-6 text-gray-900 dark:text-white"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <path
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M5 12h14M5 12l4-4m-4 4 4 4"
                    />
                </svg>
            </button>
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 my-10 lg:my-0">
                <div className="flex items-center justify-center">
                    <div className="w-full max-w-lg bg-white rounded-lg p-8 shadow-md">
                        <h1 className="text-4xl text-center mb-8">
                            Login to your account
                        </h1>
                        <form onSubmit={handelSubmit}>
                            <div className="mb-4">
                                <label htmlFor="email" className="block mb-2">
                                    Email
                                </label>
                                <input
                                    type="email"
                                    name="email"
                                    onChange={handelChangeInfo}
                                    value={info.email}
                                    className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                    id="email"
                                    placeholder="Exemple@gmail.com"
                                    pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
                                    required
                                />

                                {errors.email && (
                                    <p className="text-red-500">
                                        {errors.email}
                                    </p>
                                )}
                            </div>
                            <div className="mb-4">
                                <label
                                    htmlFor="password"
                                    className="block mb-2"
                                >
                                    Password
                                </label>
                                <input
                                    type="password"
                                    onChange={handelChangeInfo}
                                    value={info.password}
                                    className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                    id="password"
                                    name="password"
                                    placeholder="at least 8 characters"
                                />
                                {errors.password && (
                                    <p className="text-red-500">
                                        {errors.password}
                                    </p>
                                )}
                            </div>
                            <div className="mb-4">
                                <label
                                    htmlFor="remember"
                                    className="flex items-center"
                                >
                                    <input
                                        type="checkbox"
                                        id="remember"
                                        className="form-checkbox mr-2"
                                        checked={remember}
                                        onChange={handelRememberChange}
                                    />
                                    <span>Remember Me</span>
                                </label>
                            </div>
                            <div className="mb-4">
                                <a href="#" className="block text-center">
                                    Forgot password?
                                </a>
                            </div>
                            <button
                                className="py-2.5 px-5 me-2 text-sm font-medium border border-gray-200 hover:bg-opacity-800  focus:z-10 focus:ring-2  dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700 inline-flex items-center w-full rounded-lg py-2 flex justify-center bg-btn text-white"
                                type="submit"
                            >
                                <svg
                                    aria-hidden="true"
                                    id="login"
                                    role="status"
                                    className="hidden inline w-4 h-4 me-3 text-gray-200 animate-spin dark:text-gray-600"
                                    viewBox="0 0 100 101"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                                        fill="currentColor"
                                    />
                                    <path
                                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                        fill="#1C64F2"
                                    />
                                </svg>
                                Log in
                            </button>

                            <p className="text-center mt-4">
                                Don't have an account?{" "}
                                <Link onClick={useSignin}>Sign up</Link>
                            </p>
                        </form>
                    </div>
                </div>
                <div className="hidden lg:flex">
                    <img
                        src="image/Time1.png"
                        className="max-w-full"
                        alt="Image à droite du formulaire"
                    />
                </div>
            </div>
        </div>
    );
}
