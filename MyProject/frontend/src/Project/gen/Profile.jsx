import React, { useEffect, useState } from "react";
import NaveBar from "../res/NaveBar";
import { useUserContext } from "../../Context/UserContext";
import UserApi from "../../Service/Api/User/UserApi";

export default function Profile() {
    const [info, setInfo] = useState({});
    const [file, setfile] = useState({});

    const [errors, setErrors] = useState({});
    const context = useUserContext();

    const handelClickEdit = () => {
        let edit = document.getElementById("edit");
        edit.classList.toggle("hidden");
    };

    const handelChangeInput = (e) => {
        const { id, value } = e.target;
        setInfo((prevState) => ({
            ...prevState,
            [id]: value,
        }));
        console.log(id, value);
    };

    const handelChangeFile = (e) => {
        const selectedFile = e.target.files[0];
        setfile(selectedFile);
    };

    useEffect(() => {
        setInfo({
            name: context.user.name,
            password: context.user.password,
            email: context.user.email,
            password_confirmation: context.user.password,
        });
    }, []);

    const handelSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append("name", info.name);
        formData.append("email", info.email);
        formData.append("password", info.password);
        formData.append("password_confirmation", info.password_confirm);
        formData.append("image", file);
        formData.append("_method", "PUT"); // Add this line to simulate a PUT request

        // Call UpdateUser function
        await UserApi.UpdateUser(context.user.id, formData)
            .then((data) => {
                console.log(data);
                window.location.reload();
            })
            .catch((error) => {
                console.error("Error updating user:", error);
            });
    };
    return (
        <div>
            <NaveBar
                contnet={
                    <div>
                        <div className="flex flex-col items-center pb-10 w-full ">
                            {context.user.image ? (
                                <img
                                    className="w-24 h-24 mb-3 rounded-full shadow-lg"
                                    src={`http://localhost:8000/storage/${context.user.image.slice(
                                        7
                                    )}`}
                                    alt=""
                                />
                            ) : (
                                <svg
                                    className=" text-gray-800 dark:text-white w-24 h-24 mb-3 rounded-full shadow-lg rounded-full bg-gray-200"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                    viewBox="0 0 24 24"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M12 4a4 4 0 1 0 0 8 4 4 0 0 0 0-8Zm-2 9a4 4 0 0 0-4 4v1a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-1a4 4 0 0 0-4-4h-4Z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                            )}
                            <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-black">
                                {context.user.name}
                            </h5>
                            <div className="flex mt-4 md:mt-6">
                                <button
                                    onClick={handelClickEdit}
                                    className="py-2 px-4 ms-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100  dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                                >
                                    Edit profile
                                </button>
                            </div>

                            <form
                                encType="multipart/form-data"
                                onSubmit={handelSubmit}
                                className="  mx-auto w-[350px] md:w-[550px] md:mr-18 lg:w-[830px] xl:w-[1000px] container xl:mx-auto "
                            >
                                <div id="edit" className=" hidden">
                                    <div id="edit" className=" hidden">
                                        {/* Display errors */}
                                        {Object.keys(errors).length > 0 && (
                                            <div className="text-red-500 text-sm mb-4">
                                                {Object.values(errors).map(
                                                    (error, index) => (
                                                        <p key={index}>
                                                            {error}
                                                        </p>
                                                    )
                                                )}
                                            </div>
                                        )}
                                        <div className="relative z-0 w-full mb-5 group ">
                                            {/* Your existing JSX */}
                                        </div>
                                        {/* Rest of your form */}
                                    </div>
                                    <div className="relative z-0 w-full mb-5 group ">
                                        <input
                                            onChange={handelChangeInput}
                                            id="email"
                                            type="email"
                                            name="floating_email"
                                            defaultValue={context.user.email}
                                            className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-gray-600 dark:border-gray-600 dark:focus:border-btn focus:outline-none focus:ring-0 focus:border-btn peer"
                                        />
                                        <label
                                            htmlFor="floating_email"
                                            className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto peer-focus:text-btn peer-focus:dark:text-btn peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                                        >
                                            Email address
                                        </label>
                                    </div>
                                    <div className="relative z-0 w-full mb-5 group">
                                        <input
                                            onChange={handelChangeInput}
                                            id="password"
                                            type="password"
                                            name="floating_password"
                                            className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-gray-600 dark:border-gray-600 dark:focus:border-btn focus:outline-none focus:ring-0 focus:border-btn peer"
                                            placeholder=""
                                        />
                                        <label
                                            htmlFor="floating_password"
                                            className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-btn peer-focus:dark:text-btn peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                                        >
                                            Password
                                        </label>
                                    </div>
                                    <div className="relative z-0 w-full mb-5 group">
                                        <input
                                            onChange={handelChangeInput}
                                            id="password_confirm"
                                            type="password"
                                            name="repeat_password"
                                            className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-gray-600 dark:border-gray-600 dark:focus:border-btn focus:outline-none focus:ring-0 focus:border-btn peer"
                                        />
                                        <label
                                            htmlFor="floating_repeat_password"
                                            className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-btn peer-focus:dark:text-btn peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                                        >
                                            Confirm password
                                        </label>
                                    </div>
                                    <div className="grid md:grid-cols-1 md:gap-6">
                                        <div className="relative z-0 w-full mb-5 group">
                                            <input
                                                onChange={handelChangeInput}
                                                id="name"
                                                type="text"
                                                defaultValue={context.user.name}
                                                className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-gray-600 dark:border-gray-600 dark:focus:border-btn focus:outline-none focus:ring-0 focus:border-btn peer"
                                                required
                                            />
                                            <label
                                                htmlFor="floating_first_name"
                                                className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-btn peer-focus:dark:text-btn peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                                            >
                                                First name
                                            </label>
                                        </div>
                                    </div>
                                    <div className="grid md:grid-cols-1 md:gap-6">
                                        <div className="relative z-0 w-full mb-5 group">
                                            <div className="flex items-center justify-center w-full">
                                                <label
                                                    htmlFor="dropzone-file"
                                                    className="flex flex-col items-center justify-center w-full h-54 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600"
                                                >
                                                    <div className="flex flex-col items-center justify-center pt-5 pb-6">
                                                        <svg
                                                            className="w-8 h-8 mb-4 text-gray-500 dark:text-gray-400"
                                                            aria-hidden="true"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            fill="none"
                                                            viewBox="0 0 20 16"
                                                        >
                                                            <path
                                                                stroke="currentColor"
                                                                strokeLinecap="round"
                                                                strokeLinejoin="round"
                                                                strokeWidth="2"
                                                                d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"
                                                            />
                                                        </svg>
                                                        <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                                                            <span className="font-semibold">
                                                                Click to upload
                                                            </span>
                                                            your image
                                                        </p>
                                                        <p className="text-xs text-gray-500 dark:text-gray-400">
                                                            PNG, JPG ,JPEG or
                                                            GIF (MAX. 800x400px)
                                                        </p>
                                                    </div>
                                                    <input
                                                        onChange={
                                                            handelChangeFile
                                                        }
                                                        id="dropzone-file"
                                                        type="file"
                                                        name="floating_company"
                                                        className="hidden "
                                                    />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex justify-center items-center">
                                        <button
                                            type="submit"
                                            className="py-2 px-4 ms-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100  dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                                        >
                                            Save changes
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                }
            />
        </div>
    );
}
