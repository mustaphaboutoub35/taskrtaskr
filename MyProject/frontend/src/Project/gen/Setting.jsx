import React from "react";
import Dropdown1 from "../res/Dropdown";
import NaveBar from "../res/NaveBar";

function Setting() {
    return (
        <NaveBar
            contnet={
                <div className="pr-6 pl-10 pt-8  mr-8 h-screen bg-gray-50 text-medium text-gray-500 dark:text-gray-400 bg-gray-100 rounded-lg w-full ">
                    <a
                        href="#"
                        className="inline-flex items-center px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-200 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                    >
                        <svg
                            className="w-8 h-6 ml-2 me-7 text-gray-800 dark:text-black"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor"
                            viewBox="0 0 18 20"
                        >
                            <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
                        </svg>
                        Account <br />
                        Privacy, Security, change email or number
                    </a>
                    <br />
                    <br />
                    <a
                        href="#"
                        className="inline-flex items-center  px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-200 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                    >
                        <svg
                            className=" w-8 h-6 ml-2 me-7 text-gray-800 dark:text-black"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor"
                            viewBox="0 0 20 18"
                        >
                            <path d="M18 0H2a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h3.546l3.2 3.659a1 1 0 0 0 1.506 0L13.454 14H18a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-8 10H5a1 1 0 0 1 0-2h5a1 1 0 1 1 0 2Zm5-4H5a1 1 0 0 1 0-2h10a1 1 0 1 1 0 2Z" />
                        </svg>
                        Chat <br />
                        Themes, Wallpapers, Chat history
                    </a>
                    <br />
                    <br />
                    <a
                        href="#"
                        className="inline-flex items-center   px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-200 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                    >
                        <svg
                            className="w-8 h-6 ml-2 me-7 text-gray-800 dark:text-black"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 16 21"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M8 3.464V1.1m0 2.365a5.338 5.338 0 0 1 5.133 5.368v1.8c0 2.386 1.867 2.982 1.867 4.175C15 15.4 15 16 14.462 16H1.538C1 16 1 15.4 1 14.807c0-1.193 1.867-1.789 1.867-4.175v-1.8A5.338 5.338 0 0 1 8 3.464ZM4.54 16a3.48 3.48 0 0 0 6.92 0H4.54Z"
                            />
                        </svg>
                        Notifications <br />
                        Message, group & call tones
                    </a>
                    <br />
                    <br />
                    <a
                        href="#"
                        className="inline-flex items-center  px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-200 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                    >
                        <svg
                            className="w-8 h-6 ml-2 me-7 text-gray-500 dark:text-black"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor"
                            viewBox="0 0 20 20"
                        >
                            <path d="M7.824 5.937a1 1 0 0 0 .726-.312 2.042 2.042 0 0 1 2.835-.065 1 1 0 0 0 1.388-1.441 3.994 3.994 0 0 0-5.674.13 1 1 0 0 0 .725 1.688Z" />
                            <path d="M17 7A7 7 0 1 0 3 7a3 3 0 0 0-3 3v2a3 3 0 0 0 3 3h1a1 1 0 0 0 1-1V7a5 5 0 1 1 10 0v7.083A2.92 2.92 0 0 1 12.083 17H12a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h1a1.993 1.993 0 0 0 1.722-1h.361a4.92 4.92 0 0 0 4.824-4H17a3 3 0 0 0 3-3v-2a3 3 0 0 0-3-3Z" />
                        </svg>
                        Help <br />
                        Help cenre, contact use, privacy policy
                    </a>
                </div>
            }
        />
    );
}

export default Setting;
