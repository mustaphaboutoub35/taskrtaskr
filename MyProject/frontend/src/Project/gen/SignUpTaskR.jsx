import React, { useState } from "react";
import { useUserContext } from "../../Context/UserContext";
import { Link, useNavigate } from "react-router-dom";
import UserApi from "../../Service/Api/User/UserApi";

export default function SignUpTaskR() {
    const context = useUserContext();
    const navigate = useNavigate();
    const useLogin = () => {
         navigate("/Login");
    };
    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
    });

    const [errors, setErrors] = useState({});

    const handleChange = (e) => {
        const { id, value } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [id]: value,
        }));
    };

    function joinName(firstName, lastName) {
        return `${firstName} ${lastName}`;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        // Validation logic here
        const errors = {};
        if (!formData.firstName.trim()) {
            errors.firstName = "First name is required";
        }
        if (!formData.lastName.trim()) {
            errors.lastName = "Last name is required";
        }
        if (!formData.email.trim()) {
            errors.email = "Email is required";
        } else if (!/^\S+@\S+\.\S+$/.test(formData.email)) {
            errors.email = "Invalid email format";
        }
        if (formData.password.length < 8) {
            errors.password = "Password must be at least 8 characters";
        }
        if (formData.password !== formData.confirmPassword) {
            errors.confirmPassword = "Passwords do not match";
        }
        setErrors(errors);
        if (Object.keys(errors).length === 0) {
            console.log("Form submitted:", formData);
            const name = joinName(formData.firstName, formData.lastName);
            try {
                const res = await UserApi.verification().then((data)=>{
                    console.log(data)
                })
                // const response = await context
                //     .register(
                //         name,
                //         formData.email,
                //         formData.password,
                //         formData.confirmPassword
                //     )
                //     .then(() => {
                //         navigate("/login");
                //     });
            } catch (error) {
                console.error("Registration error:", error);
            }
        }
    };

    return (
        <div>
            <div className="container">
                <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 my-10 lg:my-0">
                    <div className="flex items-center justify-center">
                        <div className="w-full max-w-md bg-white rounded-lg p-10 shadow-md mt-8">
                            <h1 className="text-4xl text-center mb-8 ">
                                Sign up
                            </h1>
                            <form onSubmit={handleSubmit}>
                                <div className="mb-4">
                                    <label
                                        htmlFor="firstName"
                                        className="block mb-2"
                                    >
                                        First name
                                    </label>
                                    <input
                                        type="text"
                                        className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                        id="firstName"
                                        placeholder="First name"
                                        value={formData.firstName}
                                        onChange={handleChange}
                                    />
                                    {errors.firstName && (
                                        <p className="text-red-500">
                                            {errors.firstName}
                                        </p>
                                    )}
                                </div>
                                <div className="mb-4">
                                    <label
                                        htmlFor="lastName"
                                        className="block mb-2"
                                    >
                                        Last name
                                    </label>
                                    <input
                                        type="text"
                                        className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                        id="lastName"
                                        placeholder="Last name"
                                        value={formData.lastName}
                                        onChange={handleChange}
                                    />
                                    {errors.lastName && (
                                        <p className="text-red-500">
                                            {errors.lastName}
                                        </p>
                                    )}
                                </div>
                                <div className="mb-4">
                                    <label
                                        htmlFor="email"
                                        className="block mb-2"
                                    >
                                        Email
                                    </label>
                                    <input
                                        type="text"
                                        className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                        id="email"
                                        placeholder="Exemple@gmail.com"
                                        value={formData.email}
                                        onChange={handleChange}
                                    />
                                    {errors.email && (
                                        <p className="text-red-500">
                                            {errors.email}
                                        </p>
                                    )}
                                </div>
                                <div className="mb-4">
                                    <label
                                        htmlFor="password"
                                        className="block mb-2"
                                    >
                                        Password
                                    </label>
                                    <input
                                        type="password"
                                        className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                        id="password"
                                        placeholder="at least 8 characters"
                                        value={formData.password}
                                        onChange={handleChange}
                                    />
                                    {errors.password && (
                                        <p className="text-red-500">
                                            {errors.password}
                                        </p>
                                    )}
                                </div>
                                <div className="mb-4">
                                    <label
                                        htmlFor="confirmPassword"
                                        className="block mb-2"
                                    >
                                        Confirm password
                                    </label>
                                    <input
                                        type="password"
                                        className="border border-gray-300 rounded-lg px-4 py-2 w-full"
                                        id="confirmPassword"
                                        placeholder="at least 8 characters"
                                        value={formData.confirmPassword}
                                        onChange={handleChange}
                                    />
                                    {errors.confirmPassword && (
                                        <p className="text-red-500">
                                            {errors.confirmPassword}
                                        </p>
                                    )}
                                </div>
                                <button
                                    type="submit"
                                    className="bg-btn text-white w-full rounded-lg py-2 text-center"
                                >
                                    Sign up
                                </button>
                                <p className="text-center mt-4">
                                    Already have an account?{" "}
                                    <Link onClick={useLogin}>Log in</Link>
                                </p>
                            </form>
                        </div>
                    </div>
                    <div className="hidden lg:flex">
                        <img
                            src="image/Time1.png"
                            className="max-w-full my-20"
                            alt="Image à droite du formulaire"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}
