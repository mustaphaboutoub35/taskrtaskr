import React, { useState } from 'react'

export default function DateTasks() {
    const [today, setToday] = useState(new Date());

    setInterval(() => {
        setToday(new Date());
    }, 1000);
  return (
    <div>{today.toUTCString()}</div>
  )
}
