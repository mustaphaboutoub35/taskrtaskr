
import NaveBar from "../../res/NaveBar";
import Tasks from "./Tasks";

export default function HomeTaskeR() {
    return (
        <div>
            <NaveBar
                contnet={
                    <div className="p-6 bg-gray-50 text-medium text-gray-500 dark:text-gray-400 dark:bg-gray-800 rounded-lg w-full h-[96vh] ">
                        <Tasks />
                    </div>
                }
            />
        </div>
    );
}
