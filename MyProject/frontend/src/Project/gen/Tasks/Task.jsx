import React, { useEffect, useState } from "react";
import UpdateTache from "./UpdateTache";
import { useUserContext } from "../../../Context/UserContext";

export default function Task({ id, content, title, image, state }) {
    const [badge, setBadge] = useState("");
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const [crudModal, setCrudModal] = useState(false);
    const [forcRender, setForcRender] = useState(false);
    const [currentId, setCurrentId] = useState("");
    const context = useUserContext();
    useEffect(() => {
        if (state === "pas_encore") {
            setBadge(
                "bg-red-100 text-red-800 inline-flex items-center  me-2 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 lg:hidden"
            );
        } else if (state === "en_cours") {
            setBadge(
                "bg-blue-100 text-blue-800 inline-flex items-center  me-2 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 lg:hidden"
            );
        } else {
            setBadge(
                "bg-green-100 text-green-800 inline-flex items-center  me-2 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300 lg:hidden"
            );
        }
    }, [state]);

    const toggleDropdown = () => {
        setIsDropdownOpen(!isDropdownOpen);
    };
    const tooglecrudModal = () => {
        setCrudModal(!crudModal);
    };

    const handelReder = (par) => {
        setForcRender(par);
    };
    const handelDelete = () => {
        document.getElementById("deleteModal").classList.remove("hidden");
    };

    return (
        <div className="max-w-md mb-3 z-50 mt-2 mx-auto ">
            <div className="bg-white border border-gray-200 rounded-lg shadow p-4 dark:bg-gray-800 dark:border-gray-700 hover:bg-opacity-80">
                <div className="flex items-center justify-between mb-4 float-right ">
                    <div className="flex items-center ">
                        <span className={badge}>
                            <svg
                                className="w-2.5 h-2.5 me-1.5"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                            >
                                <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm3.982 13.982a1 1 0 0 1-1.414 0l-3.274-3.274A1.012 1.012 0 0 1 9 10V6a1 1 0 0 1 2 0v3.586l2.982 2.982a1 1 0 0 1 0 1.414Z" />
                            </svg>
                            {state}
                        </span>
                        <button>
                            <img
                                className="w-8 h-8 border-2 border-white rounded-full dark:border-gray-800 mr-3"
                                src={`http://localhost:8000/storage/${context.user.image?.slice(
                                    7
                                )}`}
                                alt=""
                            />
                        </button>
                        <button
                            data-modal-target="crud-modal"
                            data-modal-toggle="crud-modal"
                            className="dark:text-blue-500"
                            onClick={toggleDropdown}
                        >
                            <svg
                                className="w-6 h-6 text-gray-800 dark:text-white"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                            >
                                <path d="M1 5h1.424a3.228 3.228 0 0 0 6.152 0H19a1 1 0 1 0 0-2H8.576a3.228 3.228 0 0 0-6.152 0H1a1 1 0 1 0 0 2Zm18 4h-1.424a3.228 3.228 0 0 0-6.152 0H1a1 1 0 1 0 0 2h10.424a3.228 3.228 0 0 0 6.152 0H19a1 1 0 0 0 0-2Zm0 6H8.576a3.228 3.228 0 0 0-6.152 0H1a1 1 0 0 0 0 2h1.424a3.228 3.228 0 0 0 6.152 0H19a1 1 0 0 0 0-2Z" />
                            </svg>
                        </button>
                    </div>
                </div>
                <h5 className="text-md font-medium font-bold leading-none text-gray-800 dark:text-white z-0 mx-4 my-4 w-44 ">
                    {title}
                </h5>
                <div className="flow-root">
                    <ul className="divide-y divide-gray-200 dark:divide-gray-700 bg-blue-50 px-2 rounded-sm">
                        <p className="py-3 sm:py-4">
                            {content}
                            <div>
                                <hr />
                                {isDropdownOpen && (
                                    <div className=" z-50 mt-2 w-50 bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600 p-2">
                                        <div className="px-4 py-3 text-sm text-gray-900 dark:text-white">
                                            <div className="font-medium truncate">
                                                started : 10/01/2024
                                            </div>

                                            <div className="font-medium truncate">
                                                finish : 10/02/2024
                                            </div>
                                        </div>
                                        <ul className="py-2 text-sm text-gray-700 dark:text-gray-200">
                                            <li>
                                                <a
                                                    href="#"
                                                    className=" border-b-2 border-gray-200 mb-1  bg-blue-50 block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                                >
                                                    Add user
                                                </a>
                                            </li>
                                            <li>
                                                <div className=" border-b-2 border-gray-200 mb-1   bg-green-100 block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">
                                                    <UpdateTache
                                                        onReder={handelReder}
                                                        id={id}
                                                        Render={forcRender}
                                                    />
                                                </div>
                                            </li>
                                            {/* <li>
                                                <a
                                                    href="#"
                                                    className="border-b-2 border-gray-200 mb-1  bg-blue-50 block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                                                >
                                                    change state
                                                </a>
                                            </li> */}
                                        </ul>
                                        <div className="py-1">
                                            <div>
                                                <div className="flex justify-center">
                                                    <button
                                                        type="button"
                                                        className="bg-red-50 block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white w-full text-start "
                                                        onClick={() => {
                                                            setCurrentId(id);
                                                            handelDelete();
                                                        }}
                                                    >
                                                        Delete
                                                    </button>
                                                </div>

                                                <div
                                                    id="deleteModal"
                                                    tabIndex="-1"
                                                    aria-hidden="true"
                                                    className="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 flex justify-center mt-5  w-full md:inset-0 h-modal md:h-full"
                                                >
                                                    <div className="relative p-4 w-full max-w-md h-full md:h-auto">
                                                        <div className="relative p-4 text-center bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
                                                            <button
                                                                type="button"
                                                                className="text-gray-400 absolute top-2.5 right-2.5 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                                                data-modal-toggle="deleteModal"
                                                            >
                                                                <svg
                                                                    aria-hidden="true"
                                                                    id="handelX"
                                                                    onClick={() => {
                                                                        document
                                                                            .getElementById(
                                                                                "deleteModal"
                                                                            )
                                                                            .classList.add(
                                                                                "hidden"
                                                                            );
                                                                    }}
                                                                    className="w-5 h-5"
                                                                    fill="currentColor"
                                                                    viewBox="0 0 20 20"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                >
                                                                    <path
                                                                        fillRule="evenodd"
                                                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                                        clipRule="evenodd"
                                                                    ></path>
                                                                </svg>
                                                                <span className="sr-only">
                                                                    Close modal
                                                                </span>
                                                            </button>
                                                            <svg
                                                                className="text-gray-400 dark:text-gray-500 w-11 h-11 mb-3.5 mx-auto"
                                                                aria-hidden="true"
                                                                fill="currentColor"
                                                                viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path
                                                                    fillRule="evenodd"
                                                                    d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                                    clipRule="evenodd"
                                                                ></path>
                                                            </svg>
                                                            <p className="mb-4 text-gray-500 dark:text-gray-300">
                                                                Are you sure you
                                                                want to delete
                                                                this project?
                                                            </p>
                                                            <div className="flex justify-center items-center space-x-4">
                                                                <button
                                                                    onClick={() => {
                                                                        document
                                                                            .getElementById(
                                                                                "deleteModal"
                                                                            )
                                                                            .classList.add(
                                                                                "hidden"
                                                                            );
                                                                    }}
                                                                    data-modal-toggle="deleteModal"
                                                                    type="button"
                                                                    className="py-2 px-3 text-sm font-medium text-gray-500 bg-white rounded-lg border border-gray-200 hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-primary-300 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                                                                >
                                                                    No, cancel
                                                                </button>
                                                                <button
                                                                    onClick={async () => {
                                                                        await context.deleteTask(
                                                                            currentId
                                                                        );
                                                                        document
                                                                            .getElementById(
                                                                                "deleteModal"
                                                                            )
                                                                            .classList.add(
                                                                                "hidden"
                                                                            );
                                                                        window.location.reload();
                                                                    }}
                                                                    type="submit"
                                                                    className="py-2 px-3 text-sm font-medium text-center text-white bg-red-600 rounded-lg hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-500 dark:hover:bg-red-600 dark:focus:ring-red-900"
                                                                >
                                                                    Yes, I'm
                                                                    sure
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </p>
                    </ul>
                </div>
            </div>
        </div>
    );
}
