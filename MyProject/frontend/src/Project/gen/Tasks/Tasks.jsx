import React, { useEffect, useRef, useState } from "react";
import Task from "./Task";
import AjouterTache from "./AjouterTache";
import ReactPopover from "./prov/Separator";
import PlusProfile from "./prov/plusProfile";
import AddUserEmail from "./prov/AddUserEmail";
import profile1 from "../../../../public/image/profile/profile1.jpg";
import ProfileEllipse from "../../../../public/image/ProfileEllipse.png";
import ProfileEllipse2 from "../../../../public/image/ProfileEllipse2.png";
import ProfileEllipse3 from "../../../../public/image/ProfileEllipse3.png";
import ProfileRectangle9651 from "../../../../public/image/ProfileRectangle9651.png";
import UserApi from "../../../Service/Api/User/UserApi";
import { useUserContext } from "../../../Context/UserContext";
import { useParams } from "react-router-dom";
import DateTasks from "./DateTasks";

export default function Tasks() {
    const context = useUserContext();
    const { project_id } = useParams();
    const [forcRender, setForcRender] = useState(false);
    const [myProject, setProject] = useState({});
    const toggleIsGray = () => {
        document.getElementById("isGray").classList.toggle("opacity-80");
    };

    const handelReder = (par) => {
        setForcRender(par);
    };

    useEffect(() => {
        UserApi.getTasks().then(({ data }) => {
            const filtData = data.filter((e) => e.project_id === +project_id);
            context.setTasks(filtData);
            context.setEtat((prevState) => {
                return {
                    ...prevState,
                    pasEncour: filtData.filter((e) => e.state === "pas_encore"),
                    enCours: filtData.filter((e) => e.state === "en_cours"),
                    termenee: filtData.filter((e) => e.state === "terminée"),
                };
            });
        });
        UserApi.getProjectParId(project_id).then(({ data }) => {
            setProject(data);
        });
    }, [forcRender]);

    return (
        <div>
            <h3 className="text-lg font-bold text-gray-900 dark:text-white mb-2 ">
                Today
            </h3>
            <p className="mb-2 font-bold">
                <DateTasks />
            </p>
            <div className=" bg-white rounded-xl py-4 px-4">
                <div className=" grid grid-col-2 xl:grid-cols-4 items-center">
                    <h3 className="font-bold text-xl text-gray-900 xl:mb-0 mb-3 ">
                        {myProject.name}
                    </h3>
                    <p></p>
                    <div
                        className="flex bg-blue-50 p-2 rounded-lg w-[230px]"
                        aria-label="Breadcrumb"
                    >
                        <ol className="inline-flex items-center space-x-1 md:space-x-2 rtl:space-x-reverse ">
                            <div className=" mr-8 flex items-center">
                                <AddUserEmail project_id={project_id} />

                                <svg
                                    className="rtl:rotate-180 w-3 h-3 text-gray-400 mx-1 ml-3 "
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 6 10"
                                >
                                    <path
                                        stroke="currentColor"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="m1 9 4-4-4-4"
                                    />
                                </svg>
                            </div>
                            <div className="">
                                <div className="flex -space-x-2   rtl:space-x-reverse w-44 ">
                                    {context.membersUser
                                        .slice(0, 3)
                                        .map((member, index) => {
                                            return (
                                                <div key={index}>
                                                    {member.data.image ? (
                                                        <img
                                                            className="w-8 h-8 border-2 border-white rounded-full  dark:border-gray-800"
                                                            src={`http://localhost:8000/storage/${member.data.image.slice(
                                                                7
                                                            )}`}
                                                            alt=""
                                                        />
                                                    ) : (
                                                        <svg
                                                            className=" text-gray-800 dark:text-white w-8 h-8 mb-3 rounded-full shadow-lg rounded-full bg-gray-200 "
                                                            aria-hidden="true"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            width="24"
                                                            height="24"
                                                            fill="currentColor"
                                                            viewBox="0 0 24 24"
                                                        >
                                                            <path
                                                                fillRule="evenodd"
                                                                d="M12 4a4 4 0 1 0 0 8 4 4 0 0 0 0-8Zm-2 9a4 4 0 0 0-4 4v1a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-1a4 4 0 0 0-4-4h-4Z"
                                                                clipRule="evenodd"
                                                            />
                                                        </svg>
                                                    )}
                                                </div>
                                            );
                                        })}

                                    <ReactPopover
                                        trigger="click"
                                        content={
                                            <p className="">
                                                <PlusProfile
                                                    project_id={project_id}
                                                />
                                            </p>
                                        }
                                    >
                                        <button
                                            className="flex items-center justify-center w-8 h-8 text-xs font-medium text-white bg-gray-700 border-2 border-white rounded-full hover:bg-gray-600 dark:border-gray-800"
                                            href="#"
                                        >
                                            <svg
                                                className="w-6 h-6 text-white dark:text-gray-800"
                                                aria-hidden="true"
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                            >
                                                <path
                                                    stroke="currentColor"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                    strokeWidth="2"
                                                    d="M5 12h14m-7 7V5"
                                                />
                                            </svg>
                                        </button>
                                    </ReactPopover>
                                </div>
                            </div>
                            <p></p>
                            <button onClick={toggleIsGray}>
                                <AjouterTache
                                    onReder={handelReder}
                                    Render={forcRender}
                                />
                            </button>
                        </ol>
                    </div>
                </div>
                <hr className=" m-4" />
                <div className=" lg:grid grid-cols-3 inline-flex rounded-md shadow-sm w-full  grid lg::grid-cols-3 text-center bg-blue-50 hidden">
                    <div
                        href="#"
                        aria-current="page"
                        className="px-4 py-2 text-sm font-medium text-gray-900  border border-gray-200 rounded-s-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 dark:focus:ring-blue-500 dark:focus:text-white"
                    >
                        PAS ENCORE
                    </div>
                    <div
                        href="#"
                        aria-current="page"
                        className="px-4 py-2 text-sm font-medium text-gray-900  border border-gray-200 rounded-s-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 dark:focus:ring-blue-500 dark:focus:text-white"
                    >
                        EN COURS
                    </div>
                    <div
                        href="#"
                        aria-current="page"
                        className="px-4 py-2 text-sm font-medium text-gray-900  border border-gray-200 rounded-s-lg dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 dark:focus:ring-blue-500 dark:focus:text-white"
                    >
                        TERMINEE
                    </div>
                </div>
                <div className=" flex flex-row my-5 grid grid-cols-1  lg:grid-cols-3 rounded-md shadow-sm w-full bg-blue-50 py-2 ">
                    <div className="items-center mx-3">
                        {context.etat.pasEncour.map((e, key) => {
                            return (
                                <div key={key}>
                                    <Task
                                        id={e.id}
                                        title={e.title}
                                        state={e.state}
                                        image={ProfileRectangle9651}
                                        content={e.description}
                                    />
                                </div>
                            );
                        })}
                    </div>
                    <div className="items-center mx-3">
                        {context.etat.enCours.map((e, key) => {
                            return (
                                <div key={key}>
                                    <Task
                                        id={e.id}
                                        title={e.title}
                                        state={e.state}
                                        image={ProfileRectangle9651}
                                        content={e.description}
                                    />
                                </div>
                            );
                        })}
                    </div>
                    <div className="items-center mx-3">
                        {context.etat.termenee.map((e, key) => {
                            return (
                                <div key={key}>
                                    <Task
                                        id={e.id}
                                        title={e.title}
                                        state={e.state}
                                        image={ProfileRectangle9651}
                                        content={e.description}
                                    />
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
}
