import React, { useEffect, useState } from "react";
import ProfilePicture1 from "../../../../../public/image/profile/profile1.jpg";
import UserApi from "../../../../Service/Api/User/UserApi";
import { useUserContext } from "../../../../Context/UserContext";

export default function PlusProfile({ project_id }) {
    const context = useUserContext();
    const [userData, setUserData] = useState([]);
    useEffect(() => {
        UserApi.getMembers().then(async ({ data }) => {
            const filteredMembers = await data.filter((member) => {
                return (
                    member.project_id == project_id && member.accept === "true"
                );
            });
            console.log(filteredMembers);
            context.setMembers(filteredMembers);
        });
    }, [project_id]);

    useEffect(() => {
        const fetchData = async () => {
            const promises = context.members.map((member) =>
                UserApi.getUserParId(member.user_id)
            );
            const userDataArray = await Promise.all(promises);
            setUserData(userDataArray);
            context.setMembersUser(userDataArray);
        };

        fetchData();
    }, [context.members]);

    return (
        <div>
            <div className="relative w-full w-[380px] overflow-y-scroll bg-white border   border-gray-100 rounded-lg dark:bg-gray-700 dark:border-gray-600 h-52 ">
                <ul>
                    {userData.map((user, index) => (
                        <div key={index}>
                            <li className="border-b border:gray-100 dark:border-gray-600">
                                <a
                                    href="#"
                                    className="flex items-center w-full px-4 py-3 hover:bg-gray-50 dark:hover:bg-gray-800"
                                >
                                    {user.data.image ? (
                                        <img
                                            className="me-3 rounded-full w-11 h-11"
                                            src={`http://localhost:8000/storage/${user.data.image.slice(
                                                7
                                            )}`}
                                            alt=""
                                        />
                                    ) : (
                                        <svg
                                            className=" text-gray-800 dark:text-white w-16 h-11 mb-3 rounded-full shadow-lg rounded-full bg-gray-200 "
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path
                                                fillRule="evenodd"
                                                d="M12 4a4 4 0 1 0 0 8 4 4 0 0 0 0-8Zm-2 9a4 4 0 0 0-4 4v1a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-1a4 4 0 0 0-4-4h-4Z"
                                                clipRule="evenodd"
                                            />
                                        </svg>
                                    )}

                                    <p className="text-sm text-gray-500 dark:text-gray-400">
                                        <span className="font-medium text-gray-900 dark:text-white">
                                            {user.data.name}
                                        </span>
                                    </p>
                                    <div className="absolute ml-56">
                                        <span className="text-xs text-white dark:text-blue-500">
                                            <button
                                                type="button"
                                                className="px-5 py-1 text-xs font-medium bg-btn text-center text-white bg-blue-700 rounded-md hover:opacity-90 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-green-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                            >
                                                DELETE
                                            </button>
                                        </span>
                                    </div>
                                </a>
                            </li>
                        </div>
                    ))}
                </ul>
            </div>
        </div>
    );
}
