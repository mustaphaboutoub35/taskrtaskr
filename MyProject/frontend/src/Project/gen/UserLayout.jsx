import { Link, Outlet, useNavigate } from "react-router-dom";
import { useUserContext } from "../../Context/UserContext";
import { useEffect } from "react";
import UserApi from "../../Service/Api/User/UserApi";

export default function UserLayout() {
    const navigate = useNavigate();
    const context = useUserContext();
    useEffect(() => {
        if (context.authenticated === false) {
            context.logout();
            navigate("/login");
        }
    }, [context.authenticated]);

    return (
        <div>
            <Outlet />
        </div>
    );
}
