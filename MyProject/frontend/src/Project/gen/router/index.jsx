import Home from "../Home/Home";
import LoginTaskR from "../LoginTaskR";
import SignUpTaskR from "../SignUpTaskR";
import Setting from "../Setting";
import CreateProject from "../CreateProject";
import Profile from "../Profile";
import AnalyseTaskR from "../AnalyseTaskR";
import HomeTaskeR from "../Tasks/HomeTaskeR";
import { createBrowserRouter } from "react-router-dom";
import PageNotFound from "../PageNotFound";
import AddProject from "../AddProject";
import Layout from "../Layout";
import GuestLayout from "../GuestLayout";
import UserLayout from "../UserLayout";
import EditPro from "../EditPro";
import HomeTaskerGusts from "../Gest/HomeTaskerGusts";
export const router = createBrowserRouter([
    {
        element: <Layout />,
        children: [
            {
                path: "/",
                element: <Home />,
            },
            {
                path: "*",
                element: <PageNotFound />,
            },
        ],
    },
    {
        element: <GuestLayout />,
        children: [
            {
                path: "/login",
                element: <LoginTaskR />,
            },
            {
                path: "/signup",
                element: <SignUpTaskR />,
            },
        ],
    },
    {
        element: <UserLayout />,
        children: [
            {
                path: "/createProject",
                element: <CreateProject />,
            },
            {
                path: "/profile",
                element: <Profile />,
            },
            {
                path: "/analyseTaskr",
                element: <AnalyseTaskR />,
            },
            {
                path: "/tasks/:project_id",
                element: <HomeTaskeR />,
            },
            {
                path:"tasksGuest/:project_id",
                element : <HomeTaskerGusts />
            },
            {
                path: "/addProject",
                element: <AddProject />,
            },
            {
                path: "/editProject/:id",
                element: <EditPro />
            },
            {
                path: "/setting",
                element: <Setting />,
            },
        ],
    },
]);
