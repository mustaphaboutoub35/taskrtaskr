import { Dropdown } from "flowbite-react";
import UserApi from "../../Service/Api/User/UserApi";
import { useUserContext } from "../../Context/UserContext";
import { useNavigate } from "react-router-dom";

export default function Dropdown1() {
    const context = useUserContext();
    const navigate = useNavigate();
    const handelLogout = async () => {
        await UserApi.logout().then(() => {
            context.logout();
            navigate("/login");
        });
    };
    return (
        <div>
            <Dropdown label={<h1>More</h1>}>
                <Dropdown.Item onClick={() => navigate("/addproject")}>
                    Dashboard
                </Dropdown.Item>
                <Dropdown.Item onClick={() => navigate("/setting")}>
                    Settings
                </Dropdown.Item>
                <Dropdown.Item onClick={() => alert("Earnings!")}>
                    Earnings
                </Dropdown.Item>
                <Dropdown.Item onClick={() => navigate("/")}>
                    Home
                </Dropdown.Item>
                <Dropdown.Item
                    className=" hover:bg-red-300"
                    onClick={handelLogout}
                >
                    Sign out
                </Dropdown.Item>
                <Dropdown.Item className="flex md:hidden">Search</Dropdown.Item>
            </Dropdown>
        </div>
    );
}

// className='w-16 text-lg hover:text-orange'
