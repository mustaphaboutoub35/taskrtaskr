import { useEffect, useState } from "react";
import { AxiosClient } from "../../api/axios";
import { useUserContext } from "../../Context/UserContext";
import { Link, useNavigate } from "react-router-dom";
import TaskR from "../../../public/image/TaskR.png";
import UserApi from "../../Service/Api/User/UserApi";
import Notificaation from "./Notificaation";
import ReactPopover from "../gen/Tasks/prov/Separator";

export default function NaveBar({ contnet }) {
    const context = useUserContext();
    const [info, setInfo] = useState({});
    const navigate = useNavigate();
    const [members, setMembers] = useState([]);
    useEffect(() => {
        AxiosClient.get("/user")
            .then(async ({ data }) => {
                context.setUser(await data);
                setInfo(data);
            })
            .catch(() => {
                context.logout();
                navigate("/login");
            });
    }, []);

    const handelLogout = async () => {
        await UserApi.logout().then(() => {
            context.logout();
            navigate("/login");
        });
    };
    const handelProfile = () => {
        navigate("/profile");
    };
    const handelHome = () => {
        navigate("/");
    };

    const handelProjects = () => {
        navigate("/addProject");
    };
    const handelSetting = () => {
        navigate("/setting");
    };
    const handelMousDoun = () => {
        document.getElementById("btnsvg").classList.add("bg-red-500");
        document.getElementById("btnsvg").classList.add("text-white");
        document.getElementById("svg").classList.add("text-white");
    };
    const handelMousOut = () => {
        document.getElementById("btnsvg").classList.remove("bg-red-500");
        document.getElementById("btnsvg").classList.remove("text-white");
        document.getElementById("svg").classList.remove("text-white");
    };
    useEffect(() => {
        const fetchMembers = async () => {
            try {
                const { data } = await UserApi.getMembers();
                const filteredMembers = data.filter(
                    (member) =>
                        member.user_id === context.user.id &&
                        member.accept === "false"
                );
                setMembers(filteredMembers);
            } catch (error) {
                console.error("Error fetching members:", error);
            }
        };

        fetchMembers();
    }, [context.user.id]);
    return (
        <div className="">
            <div>
                <div className=" items-center grid  grid-cols-3 md:grid-cols-6 ">
                    <div>
                        <img src={TaskR} alt="" className="w-32 mx-3 mt-2" />
                    </div>
                    <div className=" flex justify-center items-center block">
                        <button>
                            <ReactPopover
                                trigger="click"
                                content={
                                    <p className="">
                                        <Notificaation />
                                    </p>
                                }
                            >
                                <button
                                    type="button"
                                    className="relative inline-flex items-center p-2 text-sm font-medium text-center text-gray-900 bg-white border border-gray-300  rounded-sm hover:bg-gray-50 focus:ring-1 focus:outline-none focus:ring-gray-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 "
                                >
                                    <svg
                                        className="w-6 h-6 text-gray-900 dark:text-gray-900"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M12 5.365V3m0 2.365a5.338 5.338 0 0 1 5.133 5.368v1.8c0 2.386 1.867 2.982 1.867 4.175 0 .593 0 1.292-.538 1.292H5.538C5 18 5 17.301 5 16.708c0-1.193 1.867-1.789 1.867-4.175v-1.8A5.338 5.338 0 0 1 12 5.365ZM8.733 18c.094.852.306 1.54.944 2.112a3.48 3.48 0 0 0 4.646 0c.638-.572 1.236-1.26 1.33-2.112h-6.92Z"
                                        />
                                    </svg>
                                    <span className="sr-only">
                                        Notifications
                                    </span>
                                    <div className="absolute inline-flex items-center justify-center w-6 h-6 text-xs font-bold text-white bg-btn border-2 border-white rounded-full -top-2 -end-2 dark:border-gray-900">
                                        {members.length}
                                    </div>
                                </button>
                            </ReactPopover>
                        </button>
                    </div>
                    <div className="mt-6 font-bold text-xl flex  items-center lg:flex hidden"></div>
                    <div className=" md:flex hidden"></div>
                    <div className="md:block hidden">
                        <div className="relative hidden md:block">
                            <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                                <svg
                                    className="w-4 h-4 text-gray-500 dark:text-gray-400"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 20 20"
                                >
                                    <path
                                        stroke="currentColor"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                                    />
                                </svg>
                                <span className="sr-only">Search icon</span>
                            </div>
                            <input
                                type="text"
                                id="search-navbar"
                                className="block w-full mt-5 p-2 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Search..."
                            />
                        </div>
                        <button
                            data-collapse-toggle="navbar-search"
                            type="button"
                            className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                            aria-controls="navbar-search"
                            aria-expanded="false"
                        ></button>
                    </div>

                    <div className="flex items-center">
                        <div className="">
                            <Link
                                to={"/profile"}
                                className="flex py-3 px-4 hover:bg-gray-100 rounded-lg dark:hover:bg-gray-600 relative top-2.5 mx-3"
                            >
                                <div className="flex-shrink-0 ">
                                    <div className=" ">
                                        {context.user.image ? (
                                            <img
                                                className="w-11 h-11 rounded-full"
                                                src={`http://localhost:8000/storage/${context.user.image.slice(
                                                    7
                                                )}`}
                                                alt=""
                                            />
                                        ) : (
                                            <svg
                                                className=" text-gray-800 dark:text-white w-11 h-11 rounded-full bg-gray-200"
                                                aria-hidden="true"
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                fill="currentColor"
                                                viewBox="0 0 24 24"
                                            >
                                                <path
                                                    fillRule="evenodd"
                                                    d="M12 4a4 4 0 1 0 0 8 4 4 0 0 0 0-8Zm-2 9a4 4 0 0 0-4 4v1a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-1a4 4 0 0 0-4-4h-4Z"
                                                    clipRule="evenodd"
                                                />
                                            </svg>
                                        )}
                                    </div>
                                </div>
                                <div className="pl-3 w-full">
                                    <div className="text-gray-500 font-normal text-sm mb-1.5 dark:text-gray-400">
                                        <span className="font-semibold text-gray-900 dark:text-white">
                                            {info.name}
                                        </span>
                                    </div>
                                    <div className="text-xs font-medium text-btn dark:text-btn">
                                        {info.email}
                                    </div>
                                </div>
                            </Link>
                        </div>
                        <div></div>
                    </div>
                </div>

                <div className="md:flex ml-7 mt-10">
                    <ul className="flex-column space-y space-y-4 text-sm font-medium text-gray-500 dark:text-gray-400 md:me-4 mb-4 md:mb-0 lg:w-64">
                        <li>
                            <button
                                onClick={handelProfile}
                                className="inline-flex items-center justify-center px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-50 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                                aria-current="page"
                            >
                                <svg
                                    className="w-6 h-4 me-2  text-gray-500 dark:text-gray-400"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                >
                                    <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
                                </svg>
                                Profile
                            </button>
                        </li>
                        <li>
                            <button
                                onClick={handelHome}
                                className="inline-flex items-center justify-center px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-50 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                                aria-current="page"
                            >
                                <svg
                                    className="w-6 h-6 me-2  text-gray-500 dark:text-gray-400"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                    viewBox="0 0 24 24"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M11.293 3.293a1 1 0 0 1 1.414 0l6 6 2 2a1 1 0 0 1-1.414 1.414L19 12.414V19a2 2 0 0 1-2 2h-3a1 1 0 0 1-1-1v-3h-2v3a1 1 0 0 1-1 1H7a2 2 0 0 1-2-2v-6.586l-.293.293a1 1 0 0 1-1.414-1.414l2-2 6-6Z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                                Home
                            </button>
                        </li>
                        <li>
                            <button
                                onClick={handelProjects}
                                className="inline-flex items-center justify-center px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-50 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                                aria-current="page"
                            >
                                <svg
                                    className="w-6 h-6 me-2  text-gray-500 dark:text-gray-400"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    fill="currentColor"
                                    viewBox="0 0 24 24"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M5.005 10.19a1 1 0 0 1 1 1v.233l5.998 3.464L18 11.423v-.232a1 1 0 1 1 2 0V12a1 1 0 0 1-.5.866l-6.997 4.042a1 1 0 0 1-1 0l-6.998-4.042a1 1 0 0 1-.5-.866v-.81a1 1 0 0 1 1-1ZM5 15.15a1 1 0 0 1 1 1v.232l5.997 3.464 5.998-3.464v-.232a1 1 0 1 1 2 0v.81a1 1 0 0 1-.5.865l-6.998 4.042a1 1 0 0 1-1 0L4.5 17.824a1 1 0 0 1-.5-.866v-.81a1 1 0 0 1 1-1Z"
                                        clipRule="evenodd"
                                    />
                                    <path d="M12.503 2.134a1 1 0 0 0-1 0L4.501 6.17A1 1 0 0 0 4.5 7.902l7.002 4.047a1 1 0 0 0 1 0l6.998-4.04a1 1 0 0 0 0-1.732l-6.997-4.042Z" />
                                </svg>
                                Projects
                            </button>
                        </li>
                        <li>
                            <button
                                onClick={handelSetting}
                                className="inline-flex items-center justify-center px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-50 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white"
                            >
                                <svg
                                    className="w-6 h-4 me-2 text-gray-500 dark:text-gray-400"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                >
                                    <path d="M18 7.5h-.423l-.452-1.09.3-.3a1.5 1.5 0 0 0 0-2.121L16.01 2.575a1.5 1.5 0 0 0-2.121 0l-.3.3-1.089-.452V2A1.5 1.5 0 0 0 11 .5H9A1.5 1.5 0 0 0 7.5 2v.423l-1.09.452-.3-.3a1.5 1.5 0 0 0-2.121 0L2.576 3.99a1.5 1.5 0 0 0 0 2.121l.3.3L2.423 7.5H2A1.5 1.5 0 0 0 .5 9v2A1.5 1.5 0 0 0 2 12.5h.423l.452 1.09-.3.3a1.5 1.5 0 0 0 0 2.121l1.415 1.413a1.5 1.5 0 0 0 2.121 0l.3-.3 1.09.452V18A1.5 1.5 0 0 0 9 19.5h2a1.5 1.5 0 0 0 1.5-1.5v-.423l1.09-.452.3.3a1.5 1.5 0 0 0 2.121 0l1.415-1.414a1.5 1.5 0 0 0 0-2.121l-.3-.3.452-1.09H18a1.5 1.5 0 0 0 1.5-1.5V9A1.5 1.5 0 0 0 18 7.5Zm-8 6a3.5 3.5 0 1 1 0-7 3.5 3.5 0 0 1 0 7Z" />
                                </svg>
                                Settings
                            </button>
                        </li>
                        <li>
                            <button
                                href="#"
                                className="inline-flex items-center justify-center px-4 py-3 rounded-lg hover:text-gray-900 bg-gray-50 hover:bg-gray-100 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white "
                            >
                                <svg
                                    className="w-6 h-4 me-2 text-gray-500 dark:text-gray-400"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                >
                                    <path d="M7.824 5.937a1 1 0 0 0 .726-.312 2.042 2.042 0 0 1 2.835-.065 1 1 0 0 0 1.388-1.441 3.994 3.994 0 0 0-5.674.13 1 1 0 0 0 .725 1.688Z" />
                                    <path d="M17 7A7 7 0 1 0 3 7a3 3 0 0 0-3 3v2a3 3 0 0 0 3 3h1a1 1 0 0 0 1-1V7a5 5 0 1 1 10 0v7.083A2.92 2.92 0 0 1 12.083 17H12a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h1a1.993 1.993 0 0 0 1.722-1h.361a4.92 4.92 0 0 0 4.824-4H17a3 3 0 0 0 3-3v-2a3 3 0 0 0-3-3Z" />
                                </svg>
                                Contact
                            </button>
                        </li>
                        <li>
                            <button
                                onClick={handelLogout}
                                onMouseEnter={handelMousDoun}
                                onMouseOut={handelMousOut}
                                id="btnsvg"
                                className="inline-flex items-center justify-center px-4 py-3 rounded-lg bg-gray-50 w-full btn-dashabord dark:hover:bg-gray-700 dark:hover:text-white hover:bg-red-500 hover:text-white"
                                aria-current="page"
                            >
                                <span className="flex items-center">
                                    <svg
                                        onMouseEnter={handelMousDoun}
                                        onMouseOut={handelMousOut}
                                        id="svg"
                                        className="w-6 h-6 me-2 text-gray-500 dark:text-gray-400 hover:bg-red-500 hover:text-white"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M4 15v2a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-2m-8 1V4m0 12-4-4m4 4 4-4"
                                        />
                                    </svg>
                                    Sign out
                                </span>
                            </button>
                        </li>
                    </ul>
                    {contnet
                    ? contnet
                    : <div>
                        
<div role="status" class="animate-pulse">
    <div class="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[640px] mb-2.5 mx-auto"></div>
    <div class="h-2.5 mx-auto bg-gray-300 rounded-full dark:bg-gray-700 max-w-[540px]"></div>
    <div class="flex items-center justify-center mt-4">
        <svg class="w-8 h-8 text-gray-200 dark:text-gray-700 me-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
            <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z"/>
        </svg>
        <div class="w-20 h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 me-3"></div>
        <div class="w-24 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
    </div>
    <span class="sr-only">Loading...</span>
</div>

                    </div> }
                </div>
            </div>
        </div>
    );
}
