import { useEffect, useState } from "react";
import UserApi from "../../Service/Api/User/UserApi";
import { useUserContext } from "../../Context/UserContext";

export default function Notificaation() {
    const context = useUserContext();
    const [members, setMembers] = useState([]);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        const fetchMembersAndProjects = async () => {
            try {
                const { data } = await UserApi.getMembers();
                const filteredMembers = data.filter(
                    (member) =>
                        member.user_id === context.user.id &&
                        member.accept === "false"
                );

                const updatedMembers = await Promise.all(
                    filteredMembers.map(async (user) => {
                        try {
                            const { data: projectData } =
                                await UserApi.getProjectParId(user.project_id);
                            const { data: userData } =
                                await UserApi.getUserParId(projectData.user_id);
                            return {
                                ...user,
                                projectName: projectData.name,
                                userName: userData.name,
                            };
                        } catch (error) {
                            console.error(
                                "Error fetching project data:",
                                error
                            );
                            return { ...user, projectName: "", userName: "" };
                        }
                    })
                );

                setMembers(updatedMembers);
            } catch (error) {
                console.error("Error fetching members:", error);
            }
        };

        fetchMembersAndProjects();
    }, [context.user.id, load]);

    const handelAccept = async (id) => {
        await UserApi.UpdateMember(id, "true").then(() => {
            setLoad(!load);
            window.location.reload();
        });
    };
    const handelAResolv = async (id) => {
        await UserApi.deleteMember(id).then(() => {
            setLoad(!load);
            window.location.reload();
        });
    };

    return (
        <div className=" w-full">
            <div className="relative w-96   overflow-y-scroll bg-white border border-gray-100 rounded-lg dark:bg-gray-700 dark:border-gray-600 h-52 ">
                <ul className="">
                    {members.length > 0 ? (
                        members.map((user, index) => (
                            <div
                                key={index}
                                className="flex justify-center items-center border border-gray-200 p-2"
                            >
                                <div>
                                    do you want to join a project
                                    <span className="bg-blue-50 text-blue-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300">
                                        {user.projectName}
                                    </span>
                                    owned by
                                    <span className="bg-blue-50 text-blue-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300">
                                        {user.userName}
                                    </span>
                                    ?
                                </div>
                                <div>
                                    <button
                                        onClick={() => {
                                            handelAccept(user.id);
                                        }}
                                        className="bg-green-100 hover:bg-green-200 text-green-800 text-xs font-semibold me-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-green-400 border border-green-400 inline-flex items-center justify-center"
                                    >
                                        accept
                                    </button>
                                </div>
                                <div>
                                    <button
                                        onClick={() => {
                                            handelAResolv(user.id);
                                        }}
                                        className="bg-blue-100 hover:bg-blue-200 text-blue-800 text-xs font-semibold me-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-blue-400 border border-blue-400 inline-flex items-center justify-center"
                                    >
                                        resolve
                                    </button>
                                </div>
                            </div>
                        ))
                    ) : (
                        <div className="flex justify-center items-center border border-gray-200 p-2">
                            you do not have a notification ...
                        </div>
                    )}
                </ul>
            </div>
        </div>
    );
}
