import { AxiosClient } from "../../../api/axios";

const UserApi = {
    gitCsrfToken : async()=>{
        return await AxiosClient.get("/sanctum/csrf-cookie",{
            baseURL : import.meta.env.VITE_BACKEND_URL
        });
    } ,
    login : async (email,password, remember = false)=>{
        let url = remember ? "/login/remember" : "/login"; 
        return await AxiosClient.post(url, {email, password});
    } ,
    register : async (name,email,password,password_confirmation )=>{
        return await AxiosClient.post("/register", {name, email, password, password_confirmation });
    } ,
    verification : async ()=>{
        return await AxiosClient.post("email/verification-notification",{email:"mustaphaboutoub35@gmail.com"})
    },
    logout : async ()=>{
        return await AxiosClient.post("/logout");
    } ,
    getUser : async ()=>{
        return await AxiosClient.get("/user");
    } ,
    getUsers : async ()=>{
        return await AxiosClient.get("/users");
    } ,
    getUserParId : async (id)=>{
        return await AxiosClient.get(`/users/${id}`);
    } ,
    getProjects : async ()=>{
        return await AxiosClient.get("/projects");
    },
    postProject : async (name,user_id)=>{
        return await AxiosClient.post("/projects",{name, user_id});
    },
    getProjectParId : async (id)=>{
        return await AxiosClient.get(`projects/${id}`);
    },
    UpdateProject : async (id,name)=>{
        return await AxiosClient.put(`/projects/${id}`,{name});
    },
    deleteProject : async (id)=>{
        return await AxiosClient.delete(`/projects/${id}`);
    },
    getTasks : async ()=>{
        return await AxiosClient.get("/taches");
    },
    postTasks : async (title,description,state,project_id)=>{
        return await AxiosClient.post("/taches",{title, description, state, project_id});
    },
    UpdateTask : async (id,title,description,state,project_id)=>{
        return await AxiosClient.put(`/taches/${id}`,{title, description, state, project_id});
    },
    UpdateGestTask : async (id,state)=>{
        return await AxiosClient.patch(`/taches/${id}`,{state});
    },
    getTasksParId : async (id)=>{
        return await AxiosClient.get(`/taches/${id}`);
    },
    deleteTask : async (id)=>{
        return await AxiosClient.delete(`/taches/${id}`);
    },
    getMembers : async ()=>{
        return await AxiosClient.get("/members");
    },
    postMember : async (user_id,project_id,accept)=>{
        return await AxiosClient.post("/members",{user_id,project_id,accept});
    },
    UpdateMember : async (id,accept)=>{
        return await AxiosClient.patch(`/members/${id}`,{accept});
    },
    deleteMember: async (id)=>{
        return await AxiosClient.delete(`/members/${id}`);
    },
    UpdateUser : async (id,formData) => {
        const response = await AxiosClient.post(`/users/${id}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data' 
            }
        });
        return response.data 
    
}
}

export default UserApi;
