<?php

use App\Http\Controllers\API\UtilisateureController;
use App\Http\Controllers\Auth\CustomLoginController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\MembersController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TachesController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

require __DIR__ . '/auth.php';

Route::apiResource("projects", ProjectController::class);
Route::apiResource("/taches", TachesController::class);
Route::apiResource("members", MembersController::class);
Route::apiResource("users", UserController::class);
Route::post('/login/remeber', [CustomLoginController::class, 'authenticate']);



